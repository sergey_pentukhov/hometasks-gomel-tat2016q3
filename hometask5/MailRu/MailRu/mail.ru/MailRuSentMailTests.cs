﻿namespace MyTests
{
    using FrameWork;
    using FrameWork.BO;
    using FrameWork.Services;
    using NUnit.Framework;
    using OpenQA.Selenium;

    [TestFixture]
    public class MailRuSentMailTests : MailRuBaseTest
    {
        public const string INCORRECT_PASSWORD = "dfdfhdgjh";
        public const string INVALID_CREDENTIALS_MESSAGE_PAGE = "Неверное имя пользователя или пароль";
        public const string TEXT_TO_BE_SENT = "Hello";
        public const string SUBJECT = "Hello";
        public const string EMAIL = "1309burbolka@mail.ru";
        public const string TEXT_ALERT = "Не указан адрес";
        public const string TEXT_NO_SUBJECT = "<Без темы>";
        public const string FULL_NAME_FILE = @"d:\TAT2016\hometask3\MailRu\Errors.txt";
        public const string NAME_FILE = "Errors.txt";

        public static string TemplateFindByIdXPath = ".//div[@class='js-checkbox b-checkbox' and @data-id='";


        private IAuthorizationService authService;
        private SendService sendService;

        public MailRuSentMailTests()
        {
            this.authService = new AuthorizationService(Site.MailRu);
            this.sendService = new SendService();
            
        }

        [Test]
        public void SuccessLogInTest()
        {
            //this.LogIn();
            var homePage = new HomeMailRuPage(this.driver);
            authService.SuccessLogin(Site.MailRu);
            Assert.That(homePage.labelUserName.GetText().Contains(Settings.LoginMailRu));
        }

        [Test]
        public void FailLogInTest()
        {
            //this.loginPage.OpenDefaultURL(Settings.SiteUrl);
            //var errorPage = this.loginPage.ErrorSignIn(Settings.Login, INCORRECT_PASSWORD);
            var errorPage = new ErrorMailRuPage(this.driver);
            this.authService.ErrorLogin(Site.MailRu);
            Assert.That(errorPage.GetErrorMessage().Contains(INVALID_CREDENTIALS_MESSAGE_PAGE));
        }

        [Test]
        public void SendMailAndCheckInboxTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            this.sendService = new SendService();
            var homePage = new HomeMailRuPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);
            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail)
                            .SendMail(pageForWriteMail);
            //var homePage2 = homePage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(SUBJECT)
            //    .TypeBodyWithClear(TEXT_TO_BE_SENT).Send();
            homePage.ClickInbox();
            Assert.That(homePage.GetSubject().Contains(this.sendService.GetMailSubject()));
            Assert.That(homePage.GetMailText().Equals(this.sendService.GetMailText()));
        }

        [Test]
        public void SendMailAndCheckOutboxTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            var homePage = new HomeMailRuPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);
            var outboxPage = new OutboxPage(this.driver);
            //var homePage2 = homePage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(SUBJECT)
            //    .TypeBodyWithClear(TEXT_TO_BE_SENT).Send();
            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail)
                                .SendMail(pageForWriteMail);

            homePage.ClickOutbox();
            Assert.That(outboxPage.GetSubject().Contains(this.sendService.GetMailSubject()));
            Assert.That(outboxPage.GetMailText().Equals(this.sendService.GetMailText()));
        }

        [Test]
        public void SendMailEmptyAdressTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            this.sendService = new SendService(LetterTypes.WithoutAdress);
            var homePage = new HomeMailRuPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);

            //var homePage2 = homePage.ClickForWriteMail().TypeTo(string.Empty).TypeSubject(SUBJECT)
            //    .TypeBody(TEXT_TO_BE_SENT).Send();
            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail)
                                .SendMail(pageForWriteMail);
            var pageForWriteMail2 = new PageForWriteMail(this.driver);

            if (pageForWriteMail2.FlagAlert)
            {
                Assert.That(pageForWriteMail2.TextAlert.Contains(TEXT_ALERT));
            }
            else
            {
                Assert.Fail("Alert isn`t presented");
            }
        }

        [Test]
        public void SendMailEmptySubjectAndBodyCheckInboxTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            this.sendService = new SendService(LetterTypes.WithoutSubjectAndBody);
            var homePage = new HomeMailRuPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);
            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail)
                                .SendMail(pageForWriteMail);
            //var homePage2 = homePage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(string.Empty)
            //    .TypeBodyWithClear(string.Empty).Send();

            var homePage3 = homePage.ClickPopup();
            var homePage4 = homePage3.ClickInbox();

            Assert.That(homePage4.GetSubject().Contains(TEXT_NO_SUBJECT));
            Assert.IsEmpty(homePage4.GetMailText());
        }

        [Test]
        public void SendMailEmptySubjectAndBodyCheckOutboxTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            this.sendService = new SendService(LetterTypes.WithoutSubjectAndBody);
            var homePage = new HomeMailRuPage(this.driver);
            var outboxPage = new OutboxPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);

            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail)
                               .SendMail(pageForWriteMail);
            //var homePage2 = homePage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(string.Empty)
            //    .TypeBodyWithClear(string.Empty).Send();

            /*var homePage3 =*/ homePage.ClickPopup().ClickOutbox();
            //var homePage4 = homePage3.ClickOutbox();
            Assert.That(outboxPage.GetSubject().Equals(TEXT_NO_SUBJECT));
            Assert.IsEmpty(outboxPage.GetMailText());
        }
 
        // Search mail by ID
        [Test]
        public void SendMailAndSaveDraftAndRemoveTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            var homePage = new HomeMailRuPage(this.driver);
            //var draftPage = homePage.ClickDraft();
            var draftPage = new DraftPage(this.driver);
            var trashPage = new TrashPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);

            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail);
            pageForWriteMail.Save();

            //var pageForWrite = draftPage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(SUBJECT)
            //                    .TypeBodyWithClear(TEXT_TO_BE_SENT).Save();

            homePage.ClickDraft();

            Assert.That(draftPage.GetSubject().Contains(this.sendService.GetMailSubject()));
            //Assert.That(draftPage.GetMailText().Equals(TEXT_TO_BE_SENT));

            string dataIdOfLetter = draftPage.GetWebDriver().FindElement(DraftPage.FirstCheckBoxLocator)
                    .GetAttribute("data-id");
            /*var draftPage3 =*/ draftPage.CheckBoxMail();
            /*var draftPage4 = */draftPage.RemoveMail();
            /*var trashPage = */draftPage.ClickTrash();

            Assert.That(trashPage.GetSubject().Contains(this.sendService.GetMailSubject()));
            //Assert.That(trashPage.GetMailText().Contains(TEXT_TO_BE_SENT));

            string currentXPathOfLetter = TemplateFindByIdXPath + dataIdOfLetter + "']";
            trashPage.GetWebDriver().FindElement(By.XPath(currentXPathOfLetter)).Click();
            trashPage.RemoveFromTrashMail();

            Assert.Throws<NoSuchElementException>(
                delegate
                {
                    trashPage.GetWebDriver().FindElement(By.XPath(currentXPathOfLetter));
                });
        }

        //// HomeTask4
        //// My letter body will be placed in iframe (by default extended format was early too)
        [Test]
        public void SendMailAttachFileTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            var homePage = new HomeMailRuPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);

            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail)
                            .FileAttach(pageForWriteMail, FULL_NAME_FILE).SendMail(pageForWriteMail);
            //var pageForWriteMail = homePage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(SUBJECT)
            //    .TypeBodyWithClear(TEXT_TO_BE_SENT).FileAttach(FULL_NAME_FILE);

            //var homePage3 = pageForWriteMail.Send();
            /*var homePage4 = */homePage.ClickInbox();
            Assert.That(homePage.GetSubject().Contains(this.sendService.GetMailSubject()));
            Assert.That(homePage.GetMailText().Equals(this.sendService.GetMailText()));
        }

        ////private HomeMailRuPage LogIn()
        ////{
        ////    //this.loginPage.OpenDefaultURL(Settings.SiteUrl);
        ////    ///* var homePage = this.loginPage.SignIn(Settings.Login, Settings.Password);
        ////    //Assert.That(homePage.GetUserName().Contains(Settings.Login));
        ////    return  new HomeMailRuPage(Browser.GetBrowser().Driver);
        ////}
    }
}
