﻿namespace MyTests
{
    using FrameWork;
    using NUnit.Framework;
    using OpenQA.Selenium;

    public class MailRuBaseTest
    {
        protected IWebDriver driver;
        protected MailRuLoginPage loginPage;

        [SetUp]
        public void FixtureBaseSetup()
        {
            this.driver = Browser.GetBrowser(Settings.BaseBrowser).Driver;
            //this.loginPage = new MailRuLoginPage(this.driver);
        }

        [TearDown]
        public void FixtureBaseTearDown()
        {
            Browser.GetBrowser().Close();
        }
    }
}
