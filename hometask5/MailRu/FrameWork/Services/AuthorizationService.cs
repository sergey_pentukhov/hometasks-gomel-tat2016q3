﻿namespace FrameWork.Services
{
    using System.Configuration;
    using FrameWork.BO;
    using Pages;

    public class AuthorizationService : IAuthorizationService
    {
        private readonly Account validAccount;

        private readonly Account wrongAccount;

        public AuthorizationService(Site service)
        {
            string login = null;
            string password = null;
            switch (service)
            {
                case Site.MailRu:
                    login = ConfigurationManager.AppSettings["LoginMailRu"];
                    password = ConfigurationManager.AppSettings["PasswordMailRu"];
                    break;
                case Site.Yandex:
                    login = ConfigurationManager.AppSettings["LoginYandex"];
                    password = ConfigurationManager.AppSettings["PasswordYandex"];
                    break;
            }

            this.validAccount = new Account(login, password);
            var random = new System.Random();
            this.wrongAccount = new Account(
                RandomString.GetRandomString(random.Next(0, 15)),
                RandomString.GetRandomString(random.Next(0, 15)));
        }

        public void SuccessLogin(Site service)
        {
            ILoginPage loginPage = null;

            switch (service)
            {
                case Site.MailRu:
                    loginPage = new MailRuLoginPage(Browser.GetBrowser().Driver);
                    break;
                case Site.Yandex:
                    loginPage = new YandexLoginPage(Browser.GetBrowser().Driver);
                    break;
            }

            loginPage.OpenDefaultURL(loginPage.StartUrl);
            loginPage.SignIn(this.validAccount);
        }

        public void ErrorLogin(Site service)
        {
            ILoginPage loginPage = null;

            switch (service)
            {
                case Site.MailRu:
                    loginPage = new MailRuLoginPage(Browser.GetBrowser().Driver);
                    break;
                case Site.Yandex:
                    loginPage = new YandexLoginPage(Browser.GetBrowser().Driver);
                    break;
            }

            loginPage.OpenDefaultURL(loginPage.StartUrl);
            loginPage.SignIn(this.wrongAccount);
        }
    }
}