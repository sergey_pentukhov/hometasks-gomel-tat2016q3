﻿namespace FrameWork.Services
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading;
    using Pages;

    public class FileService : IFileService
    {
        public byte[] ContentFile { get; set; }

        public string LocalControlSumm { get; set; }

        private const int NUMBER_TRYING = 30;

        private const int WAIT_DOWNLOAD_FILE = 10;

        public void GetContentFile(string filename)
        {
            this.ContentFile = ReadAllFile(filename);
        }

        public void UploadFile(Page page, string fullNameFile)
        {
            page.UploadFile(fullNameFile);
            bool isPopupReUploadPresent = page.IsPopupPresent();

            if (isPopupReUploadPresent)
            {
                page.ClickPopupReUpload();
            }

            page.GetWebDriver().Navigate().GoToUrl(page.GetWebDriver().Url);
        }

        public void DownloadFile(Page page, string fullNameFile)
        {
            page.PushDownload();
            this.WaitDownloadFile(fullNameFile);
        }

        public string GetMD5(byte[] hash)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hashenc = md5.ComputeHash(hash);
            var result = new StringBuilder();
            foreach (var b in hashenc)
            {
                result.Append(b.ToString("x2"));
            }

            return result.ToString();
        }

        private void WaitDownloadFile(string fullNameFile)
        {
            var timeCount = 0;
            bool exit = false;

            while (true)
            {
                var allDownloadsFiles = Directory.GetFiles(Settings.DownloadFolder);
                foreach (var f in allDownloadsFiles)
                {
                    if (f.Contains(Path.GetFileNameWithoutExtension(fullNameFile))
                        && (DateTime.Now - File.GetCreationTime(f) <= TimeSpan.FromSeconds(WAIT_DOWNLOAD_FILE)))
                    {
                        exit = true;
                        break;
                    }
                }

                if (exit)
                {
                    break;
                }

                Thread.Sleep(1000);
                timeCount++;

                if (timeCount == NUMBER_TRYING)
                {
                    throw new FileLoadException("File not download!");
                }
            }
        }

        private byte[] ReadAllFile(string filename)
        {
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                byte[] buff = new byte[file.Length];
                file.Read(buff, 0, (int)file.Length);
                return buff;
            }
        }
    }
}
