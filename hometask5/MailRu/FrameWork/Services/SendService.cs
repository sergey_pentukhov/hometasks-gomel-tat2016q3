﻿namespace FrameWork.Services
{
    using BO;
    using OpenQA.Selenium;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class SendService
    {
        private Letter mail;

        public SendService()
        {
            this.mail = LetterFactory.CreateMail(LetterTypes.Full);
        }

        public SendService(LetterTypes config)
        {
            this.mail = LetterFactory.CreateMail(config);
        }

        public string GetMailText()
        {
            return this.mail.TextOfMail;
        }

        public string GetMailSubject()
        {
            return this.mail.Topic;
        }

        public SendService ClickButtonWriteMail(Page page)
        {
            page.ClickForWriteMail();
            return this;
        }

        public SendService FillMail(PageForWriteMail pageForWriteMail)
        {
            if (this.mail.To != null)
            {
                pageForWriteMail.TypeTo(this.mail.To);
            }

            if (this.mail.Topic != null)
            {
                pageForWriteMail.TypeSubject(this.mail.Topic);
            }

            if (this.mail.TextOfMail != null)
            {
                pageForWriteMail.TypeBodyWithClear(this.mail.TextOfMail);
            }
            else
            {
                pageForWriteMail.TypeBodyWithClear(string.Empty);
            }

            return this;

        }

        public SendService FileAttach(PageForWriteMail pageForWritePage, string fullPathFile)
        {
            pageForWritePage.FileAttach(fullPathFile);
            return this;
        }

        public SendService SendMail(Page homePage)
        {
            homePage.Send();
            return this;
        }
    }
}
