﻿namespace FrameWork
{
    using System;
    using System.Configuration;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.Remote;

    public static class WebDriverFactory
    {
        private const int IMPLICITLY_WAIT_TIMEOUT = 1;
        //private static WebDriverFactory instance;

        public static IWebDriver GetWebDriver(string settingBrowser = null)
        {
            IWebDriver webDriver;
            switch (settingBrowser)
            {
                case "Firefox":
                    var profile = new FirefoxProfile();

                    var capabilities = DesiredCapabilities.Firefox();

                    profile.SetPreference("browser.download.folderList", 2);
                    profile.SetPreference("browser.download.dir", ConfigurationManager.AppSettings["DownloadFolder"]);
                    profile.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");

                    capabilities.SetCapability(FirefoxDriver.ProfileCapabilityName, profile.ToBase64String());

                    webDriver = new RemoteWebDriver(
                        new Uri("http://127.0.0.1:4444/wd/hub"), capabilities);
                    break;
                case "Chrome":
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference(
                        "download.default_directory", ConfigurationManager.AppSettings["DownloadFolder"]);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");

                    webDriver = new ChromeDriver(chromeOptions);
                    break;
                default:
                    webDriver = new RemoteWebDriver(
                        new Uri("http://127.0.0.1:4444/wd/hub"),
                                DesiredCapabilities.Firefox());
                    break;
            }

            webDriver.Manage()
                .Timeouts()
                .ImplicitlyWait(
                    TimeSpan.FromSeconds(IMPLICITLY_WAIT_TIMEOUT));

            webDriver.Manage().Window.Maximize();

            return webDriver;
        }
    }
}
