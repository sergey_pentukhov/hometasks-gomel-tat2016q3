﻿namespace FrameWork
{
    using System.Collections.ObjectModel;
    using System.Drawing;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Interactions;

    public class Element : IWebElement
    {
        private By by;

        public Element(By by)
        {
            this.by = by;
        }

        public string TagName
        {
            get
            {
                return this.FindElement(this.by).TagName;
            }
        }

        public string Text
        {
            get
            {
                return this.FindElement(this.by).Text;
            }
        }

        public bool Enabled
        {
            get
            {
                return this.FindElement(this.by).Enabled;
            }
        }

        public bool Selected
        {
            get
            {
                return this.FindElement(this.by).Selected;
            }
        }

        public Point Location
        {
            get
            {
                return this.FindElement(this.by).Location;
            }
        }

        public Size Size
        {
            get
            {
                return this.FindElement(this.by).Size;
            }
        }

        public bool Displayed
        {
            get
            {
                return this.FindElement(this.by).Displayed;
            }
        }

        public By GetBy()
        {
            return this.by;
        }

        public IWebElement GetWrappedWebElement()
        {
            return Browser.GetBrowser().FindElement(this.by);
        }

        public bool IsPresent()
        {
            return Browser.GetBrowser().IsElementPresent(this.by);
        }

        public bool IsVisible()
        {
            return Browser.GetBrowser().IsElementVisible(this.by);
        }

        public void WaitForAppear()
        {
            Browser.GetBrowser().WaitForElementIsVisible(this.by);
        }

        public string GetText()
        {
            this.WaitForAppear();
            return this.GetWrappedWebElement().Text;
        }

        public void Click()
        {
            this.WaitForAppear();
            this.GetWrappedWebElement().Click();
        }

        public void TypeValue(string value)
        {
            IWebElement element = this.GetWrappedWebElement();
            element.Clear();
            element.SendKeys(value);
        }

        public void DoubleClick()
        {
            IWebElement element = this.GetWrappedWebElement();
            Actions action = new Actions(Browser.GetBrowser().Driver);
            action.DoubleClick(element);
            action.Build().Perform();
        }

        public void Clear()
        {
            this.FindElement(this.by).Clear();
        }

        public void SendKeys(string text)
        {
            this.FindElement(this.by).SendKeys(text);
        }

        public void Submit()
        {
            this.FindElement(this.by).Submit();
        }

        public string GetAttribute(string attributeName)
        {
            return this.FindElement(this.by).GetAttribute(attributeName);
        }

        public string GetCssValue(string propertyName)
        {
            return this.FindElement(this.by).GetCssValue(propertyName);
        }

        public IWebElement FindElement(By by)
        {
            return Browser.GetBrowser().FindElement(by);
        }

        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            return Browser.GetBrowser().FindElements(by);
        }
    }
}