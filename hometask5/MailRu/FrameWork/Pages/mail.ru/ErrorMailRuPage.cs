﻿namespace FrameWork
{
    using OpenQA.Selenium;

    public class ErrorMailRuPage : Page
    {
        public static By ErrorElementLocator = By.XPath("//div[@class='b-login__errors']");

        private Element errorElement = new Element(ErrorElementLocator);

        public ErrorMailRuPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public string GetErrorMessage()
        {
            return this.errorElement.GetText();
        }
    }
}