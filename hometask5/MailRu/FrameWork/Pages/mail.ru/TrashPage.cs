﻿namespace FrameWork
{
    using OpenQA.Selenium;

    public class TrashPage : DraftPage
    {
        public static By ClearFolderLocator = By.XPath("//*[@data-name='clearFolder']");
        public static By AgreeClearFolderLocator = By.XPath("//*[@class='btn btn_stylish confirm-ok']");

        private Element clearFolder = new Element(ClearFolderLocator);
        private Element agreeClearFolder = new Element(AgreeClearFolderLocator);

        public TrashPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public DraftPage RemoveFromTrashMail()
        {
            this.clearFolder.Click();
            this.agreeClearFolder.Click();
            return new DraftPage(this.GetWebDriver());
        }
    }
}