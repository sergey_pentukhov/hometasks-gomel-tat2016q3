﻿namespace FrameWork
{
    using OpenQA.Selenium;

    public class OutboxPage : HomeMailRuPage
    {
        public static By SubjectInOutboxFirstRowLocator = By.XPath(
            ".//*[@id='b-letters']/div/div[5]/div/div[2]/div[1]/div/a/div[4]/div[3]/div[1]/div");

        public static By TextInOutboxFirstRowLocator = By.XPath(
            ".//*[@id='b-letters']/div/div[5]/div/div[2]/div[1]/div/a/div[4]/div[3]/div[1]/div/span");

        private Element subjectInOutboxFirstRow = new Element(SubjectInOutboxFirstRowLocator);
        private Element textInOutboxFirstRow = new Element(TextInOutboxFirstRowLocator);

        public OutboxPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public override string GetSubject()
        {
            return this.subjectInOutboxFirstRow.GetText().Trim();
        }

        public override string GetMailText()
        {
            return this.textInOutboxFirstRow.GetText().Trim();
        }
    }
}
