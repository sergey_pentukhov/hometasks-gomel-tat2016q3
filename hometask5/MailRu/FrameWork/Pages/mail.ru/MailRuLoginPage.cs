﻿namespace FrameWork
{
    using BO;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using Pages;
    using Services;
    using System.Configuration;

    public class MailRuLoginPage : Page, ILoginPage
    {
        public static By LoginInputLocator = By.XPath(loginXPath);
        public static By PasswordInputLocator = By.XPath(passwordXPath);
        public static By SubmitInputLocator = By.XPath(submitXPath);

        private const string loginXPath = "//*[@id='mailbox__login']";
        private const string passwordXPath = "//*[@id='mailbox__password']";
        private const string submitXPath = "//*[@id='mailbox__auth__button']";

        private Element userName = new Element(LoginInputLocator);
        private Element password = new Element(PasswordInputLocator);
        private Element submitButton = new Element(SubmitInputLocator);

        public string StartUrl { get; set; }

        public MailRuLoginPage(IWebDriver webdriver)
            : base(webdriver)
        {
            this.StartUrl = ConfigurationManager.AppSettings["DefaultUrl"];
        }

        public void SignIn(Account account)
        {
            this.userName.TypeValue(account.Login);
            this.password.TypeValue(account.Password);
            this.submitButton.Click();
        }

        public void ErrorSignIn(Account account)
        {
            this.userName.TypeValue(account.Login);
            this.password.TypeValue(account.Password);
            this.submitButton.Click();
        }
    }
}
