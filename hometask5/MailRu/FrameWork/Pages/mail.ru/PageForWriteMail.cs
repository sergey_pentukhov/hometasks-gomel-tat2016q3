﻿namespace FrameWork
{
    using OpenQA.Selenium;

    public class PageForWriteMail : Page
    {
        public static By FieldToLocator = By.XPath("//textarea[@data-original-name='To']");

        public static By FieldSubjectLocator = By.XPath("//*[@name='Subject']");
        public static By BodyLocator = By.XPath(".//*[@id='tinymce']");
      
        public static By FrameTextLocator = By.XPath(".//iframe[contains(@id,'composeEditor')]");
        public static By SaveButtonLocator = By.XPath(
            "//div[@data-name='saveDraft' and contains(@title,'(Ctrl+S)')]");

        public static By AttachLocator = By.XPath("//input[@name='Filedata']");
        public static By SavePopupLocator = By.XPath("//*[contains(text(),'Сохранено')]");

        private Element fieldTo = new Element(FieldToLocator);
        private Element fieldSubject = new Element(FieldSubjectLocator);
        private Element bodyField = new Element(BodyLocator);
        private Element frameText = new Element(FrameTextLocator);
        private Element saveButton = new Element(SaveButtonLocator);

        public PageForWriteMail(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public PageForWriteMail TypeTo(string email)
        {
            this.fieldTo.TypeValue(email);
            return this;
        }

        public PageForWriteMail TypeSubject(string subject)
        {
            this.fieldSubject.TypeValue(subject);
            return this;
        }

        public PageForWriteMail TypeBody(string body)
        {
            this.GetWebDriver().SwitchTo()
                .Frame(this.GetWebDriver().FindElement(FrameTextLocator));
            this.bodyField.TypeValue(body);
            this.GetWebDriver().SwitchTo().ParentFrame();
            return this;
        }

        public PageForWriteMail TypeBodyWithClear(string body)
        {
            this.GetWebDriver().SwitchTo()
                .Frame(this.GetWebDriver().FindElement(FrameTextLocator));
            this.GetWebDriver().FindElement(BodyLocator).Clear();
            this.bodyField.TypeValue(body);
            this.GetWebDriver().SwitchTo().ParentFrame();
            return this;
        }

        public PageForWriteMail FileAttach(string fullPathFile)
        {
            this.GetWebDriver().FindElementExtention(AttachLocator, FIND_TIMEOUT_SECONDS).SendKeys(fullPathFile);
            return new PageForWriteMail(this.GetWebDriver());
        }



        public PageForWriteMail Save()
        {
            this.saveButton.Click();
            Browser.GetBrowser().WaitForElementIsVisible(SavePopupLocator);
            return new PageForWriteMail(this.GetWebDriver());
        }
    }
}