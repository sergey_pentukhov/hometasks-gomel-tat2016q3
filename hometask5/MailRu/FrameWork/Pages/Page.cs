﻿namespace FrameWork
{
    using System;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

#pragma warning disable SA1649 // File name must match first type name
    public static class PageExtensions
#pragma warning restore SA1649 // File name must match first type name
    {
        /// <summary>
        /// Find an element, waiting until a timeout is reached if necessary.
        /// </summary>
        /// <param name="context">The search context.</param>
        /// <param name="by">Method to find elements.</param>
        /// <param name="timeout">How many seconds to wait.</param>
        /// <param name="displayed">Require the element to be displayed?</param>
        /// <returns>The found element.</returns>
        public static IWebElement FindElementExtention(this ISearchContext context, By by, uint timeout, bool displayed = false)
        {
            var wait = new DefaultWait<ISearchContext>(context);
            wait.Timeout = TimeSpan.FromSeconds(timeout);
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait.Until(ctx =>
            {
                var elem = ctx.FindElement(by);
                if (displayed && !elem.Displayed)
                {
                    return null;
                }

                return elem;
            });
        }
    }

    public abstract class Page
    {
        public bool FlagAlert;
        public string TextAlert;
        public static By UploadFileButtonLocator = By.XPath("//input[@class='button__attach']");
        public static By PopupReUploadLocator = By.XPath("//div[contains(text(),'Загрузка')]");
        public static By DownloadButtonLocator = By.XPath(".//*[@data-click-action='resource.download']");
        public static By WriteMailButtonLocator = By.XPath("//a[@data-name='compose']");
        public static By SendButtonLocator = By.XPath(
          ".//*[@id='b-toolbar__right']//*[@class='b-toolbar__btn b-toolbar__btn_false js-shortcut']");

        protected const int PAGE_LOAD_TIMEOUT_SECONDS = 60;
        protected const int FIND_TIMEOUT_SECONDS = 1;
        protected const int ELEMENT_VISIBILITY_TIMEOUT_SECONDS = 15;

        private IWebDriver driver;
        private Element uploadFileButton = new Element(UploadFileButtonLocator);
        private Element popupReUpload = new Element(PopupReUploadLocator);
        private Element downloadButton = new Element(DownloadButtonLocator);
        private Element writeMailButton = new Element(WriteMailButtonLocator);
        private Element sendButton = new Element(SendButtonLocator);

        public Page(IWebDriver webDriver)
        {
            this.driver = webDriver;

            FlagAlert = Browser.GetBrowser().AlertIsPresent();
            if (FlagAlert)
            {
                TextAlert = Browser.GetBrowser().GetAlertText();
            }
            
            this.driver.Manage().Timeouts().SetPageLoadTimeout(
                                TimeSpan.FromSeconds(PAGE_LOAD_TIMEOUT_SECONDS));
        }

        public virtual void Send()
        {
            this.sendButton.Click();
        }

        public virtual void UploadFile(string fullPathFile)
        {
            this.uploadFileButton.TypeValue(fullPathFile);
        }

        public virtual bool IsPopupPresent()
        {
            bool isPopupReUploadPresent;
            try
            {
                this.popupReUpload.WaitForAppear();
                isPopupReUploadPresent = true;
            }
            catch (NoSuchElementException)
            {
                isPopupReUploadPresent = false;
            }

            return isPopupReUploadPresent;
        }

        public virtual void ClickPopupReUpload()
        {
            this.popupReUpload.Click();
        }

        public virtual void PushDownload()
        {
            this.downloadButton.Click();
        }

        public IWebDriver GetWebDriver()
        {
            return this.driver;
        }

        public void OpenDefaultURL(string startResourseUrl)
        {
            //bool isAlertPresent;

            Browser.GetBrowser().Open(startResourseUrl);

            //try
            //{
            //    this.GetWebDriver().SwitchTo().Alert();
            //    isAlertPresent = true;
            //}
            //catch (Exception)
            //{
            //    isAlertPresent = false;
            //}

            if (Browser.GetBrowser().AlertIsPresent())
            {
                this.GetWebDriver().SwitchTo().Alert().Dismiss();
            }
        }

        public void ClickForWriteMail()
        {
            this.writeMailButton.Click();
        }
        //public void WaitForElementsAppear(By locator)
        //{
        //    new WebDriverWait(this.driver, TimeSpan.FromSeconds(ELEMENT_VISIBILITY_TIMEOUT_SECONDS))
        //                    .Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(locator));
        //}

        //public void WaitForElementsDisAppear(By locator)
        //{
        //    new WebDriverWait(this.driver, TimeSpan.FromSeconds(ELEMENT_VISIBILITY_TIMEOUT_SECONDS))
        //                    .Until(ExpectedConditions.InvisibilityOfElementLocated(locator));
        //}

        //public void WaitForElementsIsPresent(By locator)
        //{
        //    new WebDriverWait(this.driver, TimeSpan.FromSeconds(ELEMENT_VISIBILITY_TIMEOUT_SECONDS))
        //                .Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(locator));
        //}

        //public bool IsVisible(IWebElement element)
        //{
        //    try
        //    {
        //        return element.Displayed;
        //    }
        //    catch (StaleElementReferenceException)
        //    {
        //        return false;
        //    }
        //}
    }
}
