﻿using FrameWork.BO;
using FrameWork.Services;

namespace FrameWork.Pages
{
    public interface ILoginPage
    {
        void ErrorSignIn(Account account);
        void SignIn(Account account);
        void OpenDefaultURL(string url);
        string StartUrl { get; set; }
    }
}