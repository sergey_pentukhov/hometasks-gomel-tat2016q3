﻿namespace FrameWork.BO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class Letter
    {
        private string to;
        private string topic;
        private string textOfMail;
        //private string pathToFile;
        //private TimeSpan timestamp;

        public Letter(string to = null, string topic = null, string textOfMail = null/*, string pathToFile*/)
        {
            this.to = to;
            this.topic = topic;
            this.textOfMail = textOfMail;
            //this.pathToFile = pathToFile;
        }

        public string To
        {
            get { return this.to; }
            set { this.to = value; }
        }

        public string Topic
        {
            get { return this.topic; }
            set { this.topic = value; }
        }

        //public string PathToFile
        //{
        //    get { return this.pathToFile; }
        //    set { this.pathToFile = value; }
        //}

        public string TextOfMail
        {
            get { return this.textOfMail; }
            set { this.textOfMail = value; }
        }

        //public TimeSpan Timestamp
        //{
        //    get { return this.timestamp; }
        //    set { this.timestamp = value; }
        //}

    }
}
