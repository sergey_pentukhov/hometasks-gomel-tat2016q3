﻿using NUnit.Framework;
using System;

namespace nunit_testing_framework.Tests
{
    [TestFixture(Category = "Enum")]
    class UnitTestEnum
    {
        [Test(Description = "Test real months")]
        public void TestEnumMonth()
        {
            Assert.That(Enum.GetValues(typeof(Month)).Length == 12);
        }

        [Test(Description = "Test amount mood")]
        public void TestEnumMood()
        {
            Assert.That(Enum.GetValues(typeof(Mood)).Length == 6);
        }
    }
}
