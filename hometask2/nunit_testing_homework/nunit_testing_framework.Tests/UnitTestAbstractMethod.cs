﻿using NUnit.Framework;
using System;

namespace nunit_testing_framework.Tests
{
    [TestFixture(Category = "Human")]
    class UnitTestAbstractMethod
    {
        [Test(Description = "Exist method GetMood")]
        public void TestGetMood()
        {
            var boolAbstract = (typeof(Human)).IsAbstract; 
            Assert.AreEqual(true, boolAbstract);
        }
    }
}
