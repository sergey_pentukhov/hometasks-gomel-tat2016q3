﻿using NUnit.Framework;
using nunit_testing_framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nunit_testing_framework.Tests
{
    [TestFixture(Category = "Girl")]
    public class UnitTestClassGirl
    {
        private Boy _boy;
        private Girl _girl;
        const double AMOUNT_FOR_SPENDING = 99000001;
        const double WRONG_AMOUNT_FOR_SPENDING = -8000;

        [SetUp]
        public void TestInit()
        {
            _boy = new Boy(Month.June, 100000000.0, null);
            _girl = new Girl(true, true, null);
        }

        [Ignore("Waiting for Joe to fix his bugs", Until = "2016-12-31 12:00:00Z")]
        [Test(Author = "Pentukhou Sergey")]
        public void FakeGirlTest()
        {
            Assert.Fail();
        }

        [Category("CreateInstance")]
        [TestCase(true, false, null)]
        [TestCase(false, false, null)]
        [TestCase(false, true, null)]
        [TestCase(true, true, null)]
        public void TestCreateInstancesClassGirlTreeParametersAndNulBoyFriend(bool isPretty,
                                                bool isSlimFriendGotAFewKilos, Boy boyFriend)
        {
            var localGirl = new Girl(isPretty, isSlimFriendGotAFewKilos, boyFriend);
            Assert.IsInstanceOf(typeof(Girl), localGirl);
        }

        [Category("CreateInstance")]
        [TestCase( true, false, Month.January, 1000000000.0, null)]
        [TestCase( false, false, Month.December, 50000.0, null)]
        [TestCase( false, true, 11, -999999999.0, null)]
        [TestCase( true, true, 0, 1000000, null)]
        public void TestCreateInstancesClassGirlTreeParametersAndBoyFriend( bool isPretty,
                    bool isSlimFriendGotAFewKilos, Month birthdayMonth, double wealth, Girl girlFriend)
        {
            var localBoy = new Boy(birthdayMonth, wealth, girlFriend);
            var localGirl = new Girl(isPretty, isSlimFriendGotAFewKilos, localBoy);
            Assert.IsInstanceOf(typeof(Girl), localGirl);
            Assert.AreEqual(localBoy, localGirl.BoyFriend);
        }

        [Category("CreateInstance")]
        [TestCase(true, false)]
        [TestCase(false, false)]
        [TestCase(false, true)]
        [TestCase(true, true)]
        public void TestCreateInstancesClassGirlTwoParameters(bool isPretty,
                                                bool isSlimFriendGotAFewKilos)
        {
            var localGirl = new Girl(isPretty, isSlimFriendGotAFewKilos);
            Assert.IsInstanceOf(typeof(Girl), localGirl);
        }

        [Category("CreateInstance")]
        [TestCase(true)]
        [TestCase(false)]
        public void TestCreateInstancesClassGirlOneParameter(bool isPretty)
        {
            var localGirl = new Girl(isPretty);
            Assert.IsInstanceOf(typeof(Girl), localGirl);
        }

        [Category("CreateInstance")]
        [Test]
        public void TestCreateInstancesClassGirl()
        {
            var localGirl = new Girl();
            Assert.IsInstanceOf(typeof(Girl), localGirl);
        }

        [Category("My")]
        [Test]
        public void TestSetMyselfAsGirlFriendToMyBoyFriend()
        {
            Assert.AreNotSame(_girl, _boy.GirlFriend);
            _girl.BoyFriend = _boy;
            _girl.SetMyselfAsGirlFriendToMyBoyFriend();
            Assert.AreSame(_girl, _boy.GirlFriend);
        }

        [Category("BoolMethod")]
        [Test]
        public void TestIsBoyFriendRich()
        {
            Assert.That(!_girl.IsBoyFriendRich());
            _girl.BoyFriend = _boy;
            Assert.That(_girl.IsBoyFriendRich());
            _girl.BoyFriend = null;
            Assert.That(!_girl.IsBoyFriendRich());
        }

        [Category("MethodSpendMoney")]
        [Test, TestCaseSource("DoubleValues")]
        public void TestSpendBoyFriendMoney(double amountForSpending)
        {
            _girl.BoyFriend = _boy;
            _girl.SpendBoyFriendMoney(amountForSpending);
            Assert.That(_boy.IsRich());
        }

        static double[] DoubleValues = { 10000000, 99000000, 0 };

        [Category("MethodSpendMoney")]
        [Test]
        public void TestRestBoySomeMoneyAndTestNegativeIsRich()
        {
            _girl.BoyFriend = _boy;
            _girl.SpendBoyFriendMoney(AMOUNT_FOR_SPENDING);
            Assert.That(!_boy.IsRich());
        }

        [Category("MethodSpendMoney")]
        [Test]
        public void TestArgumentExceptionSpendSomeMoney()
        {
            _girl.BoyFriend = _boy;

            var ex = Assert.Throws<ArgumentException>(delegate 
                            { _girl.SpendBoyFriendMoney(WRONG_AMOUNT_FOR_SPENDING); });

            StringAssert.IsMatch(@"You can't spend negative value\$"
                , ex.Message);
        }

        [Category("BoolMethod")]
        [Test]
        public void TestIsBoyFriendWillBuyNewShoes()
        {
            _girl.BoyFriend = _boy;
            var boolValue = _girl.IsBoyFriendWillBuyNewShoes();
            Assert.AreEqual(true, boolValue);
        }

        [Category("BoolMethod")]
        [Test]
        public void TestIsSlimFriendBecameFat()
        {
            Assert.AreEqual(false, !_girl.IsPretty);
            var localGirl = new Girl(false, true);
            var actualBool = localGirl.IsSlimFriendBecameFat();
            Assert.AreEqual(true, actualBool);
        }

        [Category("Mood")]
        [Test]
        public void TestGetMood()
        {
            _girl.BoyFriend = _boy;
            var girlMood = _girl.GetMood();
            Assert.AreEqual(girlMood, Mood.Excellent);

            _girl.SpendBoyFriendMoney(AMOUNT_FOR_SPENDING);
            girlMood = _girl.GetMood();
            Assert.AreEqual(girlMood, Mood.Good);

            var localGirl = new Girl(false, true, null);
            localGirl.BoyFriend = new Boy(Month.August, 1000000, localGirl);
            girlMood = localGirl.GetMood();
            Assert.AreEqual(girlMood, Mood.Good);

            localGirl.SpendBoyFriendMoney(990000);
            girlMood = localGirl.GetMood();
            Assert.AreEqual(girlMood, Mood.Neutral);


            var localGirl2 = new Girl(false, false, null);
            localGirl2.BoyFriend = new Boy(Month.August, 1, localGirl2);
            girlMood = localGirl2.GetMood();
            Assert.AreEqual(girlMood, Mood.IHateThemAll);
        }


    }
}