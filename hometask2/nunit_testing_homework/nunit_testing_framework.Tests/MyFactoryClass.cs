﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace nunit_testing_framework.Tests
{
    public class MyFactoryClass
    {
        [TestCaseFactory(typeof(int), typeof(int))]
        public static IEnumerable TestCases
        {
            get
            {
                yield return new TestCaseData(12, 3).Returns(4);
                yield return new TestCaseData(12, 2).Returns(6);
                yield return new TestCaseData(12, 4).Returns(3);
                yield return new TestCaseData(0, 0)
                  .Throws(typeof(DivideByZeroException))
                  .With.TestName("DivideByZero")
                  .With.Description("An exception is expected");
            }
        }
    }
}
