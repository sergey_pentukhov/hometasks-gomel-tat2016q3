﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace nunit_testing_framework.Tests
{

    public class BaseUnitTestClass
    {

        private Girl _girl;
        private Boy _boy;

        [SetUp]
        public void TestInit()
        {
            _boy = new Boy(Month.June, 100000000.0, null);
            _girl = new Girl(true, true, null);
        }
    }
}
