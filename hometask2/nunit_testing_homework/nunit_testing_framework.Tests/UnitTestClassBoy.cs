﻿using System;
using NUnit.Framework;
using System.Collections;

namespace nunit_testing_framework.Tests
{

    [TestFixture(Category = "Boy")]
    public class UnitTestClassBoy
    {
        private Girl _girl;
        private Boy _boy;
        const double AMOUNT_FOR_SPENDING = 99000001;
        const double WRONG_AMOUNT_FOR_SPENDING = -8000;

        [SetUp]
        public void TestInit()
        {
            _boy = new Boy(Month.June, 100000000.0, null);
            _girl = new Girl(true, true, null);
        }

        [Category("CreateInstance")]
        [TestCase(0, 50000.0, null)]
        [TestCase(Month.December, 50000.0, null)]
        [TestCase(0, 0.0, null)]
        [TestCase(7, -1000000.0, null)]
        [TestCase(Month.January, 1000000000.0, null)]
        public void TestCreateInstancesClassBoyTreeParametersAndNullGirlFriend(Month enumMonth, double wealth,
                                                                                Girl girlFriend)
        {
            var localBoy = new Boy(enumMonth, wealth, girlFriend);
            Assert.IsInstanceOf(typeof(Boy), localBoy);
        }

        [Category("CreateInstance")]
        [TestCase(Month.January, 1000000000.0, true, true, null)]
        [TestCase(Month.December, 50000.0, false, true, null)]
        [TestCase(11, -999999999.0, false, false, null)]
        public void TestCreateInstancesClassBoyTreeParametersAndGirlFriend(Month birthdayMonth, double wealth,
                                                bool isPretty, bool isSlimFriendGotAFewKilos, Boy boyFriend)
        {
            var localGirl = new Girl(isPretty, isSlimFriendGotAFewKilos, boyFriend);
            var localBoy = new Boy(birthdayMonth, wealth, localGirl);
            Assert.IsInstanceOf(typeof(Boy), localBoy);
            Assert.AreEqual(localGirl, localBoy.GirlFriend);
        }

        [Category("CreateInstance")]
        [TestCase(Month.January, 1000000000)]
        [TestCase(Month.May, 0.0)]
        [TestCase(5, -99666669.0)]
        public void TestCreateInstancesClassBoyTwoParameters(Month birthdayMonth, double wealth)
        {
            var localBoy = new Boy(birthdayMonth, wealth);
            Assert.IsInstanceOf(typeof(Boy), localBoy);
        }

        [Category("CreateInstance")]
        [TestCase(Month.August)]
        [TestCase(11)]
        [TestCase(5)]
        public void TestCreateInstancesClassBoyOneParameter(Month birthdayMonth)
        {
            var localBoy = new Boy(birthdayMonth);
            Assert.IsInstanceOf(typeof(Boy), localBoy);
        }

        [Category("CreateInstance")]
        [Test]
        public void TestCreateInstancesClassBoy()
        {
            var localBoy = new Boy();
            Assert.IsInstanceOf(typeof(Boy), localBoy);
        }

        [Category("My")]
        [Test]
        public void TestSetMyselfAsBoyFriendToMyGirlFriend()
        {
            Assert.AreNotSame(_boy, _girl.BoyFriend);
            _boy.GirlFriend = _girl;
            _boy.SetMyselfAsBoyFriendToMyGirlFriend();
            Assert.AreSame(_boy, _girl.BoyFriend);
        }

        [Category("BoolMethod")]
        [TestCase(Month.June)]
        [TestCase(Month.July)]
        [TestCase(Month.August)]
        public void TestIsSummerMonth(Month birthdayMonth)
        {
            var localBoy = new Boy(birthdayMonth, 0, null);
            Assert.That(localBoy.IsSummerMonth());
        }

        [Category("BoolMethod")]
        [TestCase(Month.January)]
        [TestCase(Month.February)]
        [TestCase(Month.March)]
        [TestCase(Month.April)]
        [TestCase(Month.May)]
        [TestCase(Month.September)]
        [TestCase(Month.October)]
        [TestCase(Month.November)]
        [TestCase(Month.December)]
        public void TestIsNotSummerMonth(Month birthdayMonth)
        {
            var localBoy = new Boy(birthdayMonth,0,null);
            Assert.That(!localBoy.IsSummerMonth());
        }


        [Category("BoolMethod")]
        [Test, TestCaseSource("DoubleValues")]
        public void TestIsRich(double amountForSpending)
        {
            _boy.SpendSomeMoney(amountForSpending);
            Assert.That(_boy.IsRich());
        }

        static double[] DoubleValues = { 10000000, 99000000 , 0};



        [Category("MethodSpendMoney")]
        [Test]
        public void TestRestSpendSomeMoneyAndTestNegativeIsRich()
        {
            _boy.SpendSomeMoney(AMOUNT_FOR_SPENDING);
            Assert.That(!_boy.IsRich());
        }

        [Category("MethodSpendMoney")]
        [Test]
        public void TestNullReferenceExceptionSpendSomeMoney()
        {
            _boy.SpendSomeMoney(AMOUNT_FOR_SPENDING);

            var ex = Assert.Throws<NullReferenceException>(delegate { _boy.SpendSomeMoney(AMOUNT_FOR_SPENDING); });

            StringAssert.IsMatch(@"Not enough money! Requested amount is \d*\$ but you can't spend more then \d*\$"
                , ex.Message);
        }

        [Category("MethodSpendMoney")]
        [Test]
        public void TestArgumentExceptionSpendSomeMoney()
        {
            var ex = Assert.Throws<ArgumentException>(delegate { _boy.SpendSomeMoney(WRONG_AMOUNT_FOR_SPENDING); });

            StringAssert.IsMatch(@"You can't spend negative value\$"
                , ex.Message);
        }

        [Category("BoolMethod")]
        [Test]
        public void TestIsPrettyGirlFriend()
        {
            Assert.That(!_boy.IsPrettyGirlFriend());
            _boy.GirlFriend = _girl;
            Assert.That(_boy.IsPrettyGirlFriend());
            _boy.GirlFriend = null;
            Assert.That(!_boy.IsPrettyGirlFriend());
        }

        [Category("Mood")]
        [Test]
        public void TestGetMood()
        {
            _boy.GirlFriend = _girl;
            var boyMood = _boy.GetMood();
            Assert.AreEqual(boyMood, Mood.Excellent);

            _boy.GirlFriend = _girl;
            var localBoy = new Boy(0, 100000000, _girl);
            boyMood = localBoy.GetMood();
            Assert.AreEqual(boyMood, Mood.Good);

            var localGirl = new Girl(false, true, null);
            _boy.GirlFriend = localGirl;
            boyMood = _boy.GetMood();
            Assert.AreEqual(boyMood, Mood.Neutral);

            //    return Mood.Bad;
            localBoy.GirlFriend = localGirl;
            boyMood = localBoy.GetMood();
            Assert.AreEqual(boyMood, Mood.Bad);

            var localPoorSummerBoy = new Boy(Month.August, 5, localGirl);
            boyMood = localPoorSummerBoy.GetMood();
            Assert.AreEqual(boyMood, Mood.Bad);

            var localPoorWithPrettyGirlBoy = new Boy(Month.February, 5, _girl);
            boyMood = localPoorWithPrettyGirlBoy.GetMood();
            Assert.AreEqual(boyMood, Mood.Bad);

            //    return Mood.Horrible;
            localPoorWithPrettyGirlBoy.GirlFriend = null;
            boyMood = localPoorWithPrettyGirlBoy.GetMood();
            Assert.AreEqual(boyMood, Mood.Horrible);

        }
    }
}
