﻿namespace nunit_testing_framework
{
    public enum Mood
    {
        Excellent, Good, Neutral, Bad, Horrible, IHateThemAll
    }
}