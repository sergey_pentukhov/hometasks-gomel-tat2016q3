﻿using System;

namespace nunit_testing_framework
{
    public class Boy : Human
    {
        private Month BirthdayMonth { get; }

        public Girl GirlFriend { get; set; }

        private double Wealth { get; set; }

        public Boy(Month birthdayMonth, double wealth, Girl girlFriend)
        {
            this.BirthdayMonth = birthdayMonth;
            this.Wealth = wealth;
            this.GirlFriend = girlFriend;
            //this.GirlFriend.BoyFriend = this;

        }

        public Boy(Month birthdayMonth, double wealth) : this (birthdayMonth, wealth, null)
        {
        }


        public Boy(Month birthdayMonth) : this(birthdayMonth, 0, null)
        {
            this.BirthdayMonth = birthdayMonth;
            this.Wealth = 0;
            this.GirlFriend = null;
        }

        //-----------------------------
        public Boy() : this(0, 0.0, null)
        {
        }

        public void SetMyselfAsBoyFriendToMyGirlFriend()
        {
            if (GirlFriend != null)
                this.GirlFriend.BoyFriend = this;
        }

        public override Mood GetMood()
        {
            if (IsRich() && IsPrettyGirlFriend() && IsSummerMonth())
            {
                return Mood.Excellent;
            }
            else if (IsRich() && IsPrettyGirlFriend())
            {
                return Mood.Good;
            }
            else if (IsRich() && IsSummerMonth())
            {
                return Mood.Neutral;
            }
            else if (IsRich() || IsPrettyGirlFriend() || IsSummerMonth())
            {
                return Mood.Bad;
            }
            return Mood.Horrible;
        }

        public void SpendSomeMoney(double amountForSpending)
        {
            if (amountForSpending < 0)
            {
                throw new ArgumentException(
                                   String.Format(
                                       $"You can't spend negative value$"));
            }

            if (amountForSpending <= Wealth)
            {
                Wealth -= amountForSpending;
            }
            else
            {
                throw new NullReferenceException(
                    String.Format(
                        $"Not enough money! Requested amount is {amountForSpending}$ but you can't spend more then {Wealth}$"));
            }
        }

        public bool IsSummerMonth()
        {
            return Month.June.Equals(BirthdayMonth) ||
                   Month.July.Equals(BirthdayMonth) || Month.August.Equals(BirthdayMonth);
        }

        public bool IsRich()
        {
            return Wealth >= 1000000;
        }

        public bool IsPrettyGirlFriend()
        {
            return GirlFriend != null && GirlFriend.IsPretty;
        }
    }
}