﻿namespace nunit_testing_framework
{
    public abstract class Human
    {
        public abstract Mood GetMood();
    }
}