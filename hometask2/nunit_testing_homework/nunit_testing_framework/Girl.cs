﻿using System;

namespace nunit_testing_framework
{
    public class Girl : Human
    {
        public bool IsPretty { get; }
        public Boy BoyFriend { get; set; }
        private bool IsSlimFriendGotAFewKilos { get; }

        public Girl(bool isPretty, bool isSlimFriendGotAFewKilos, Boy boyFriend)
        {
            this.IsPretty = isPretty;
            this.IsSlimFriendGotAFewKilos = isSlimFriendGotAFewKilos;
            this.BoyFriend = boyFriend;
            //this.BoyFriend.GirlFriend = this;
        }

        public Girl(bool isPretty, bool isSlimFriendGotAFewKilos) : this (isPretty, isSlimFriendGotAFewKilos, null)
        {            
        }

        public Girl(bool isPretty) : this(isPretty, false, null)
        {
        }

        public Girl() : this(false, true, null)
        {
        }

        public void SetMyselfAsGirlFriendToMyBoyFriend()
        {
            if (BoyFriend != null)
                this.BoyFriend.GirlFriend = this;
        }

        public override Mood GetMood()
        {
            if (IsBoyFriendWillBuyNewShoes())
            {
                return Mood.Excellent;
            }
            else if (IsPretty || IsBoyFriendRich())
            {
                return Mood.Good;
            }
            else if (IsSlimFriendBecameFat())
            {
                return Mood.Neutral;
            }
            return Mood.IHateThemAll;
        }

        public void SpendBoyFriendMoney(double amountForSpending)
        {
            if (amountForSpending < 0)
            {
                throw new ArgumentException(
                                   String.Format(
                                       $"You can't spend negative value$"));
            }

            if (IsBoyFriendRich())
            {
                BoyFriend.SpendSomeMoney(amountForSpending);
            }
        }

        public bool IsBoyFriendRich()
        {
            return BoyFriend != null && BoyFriend.IsRich();
        }

        public bool IsBoyFriendWillBuyNewShoes()
        {
            return IsBoyFriendRich() && IsPretty;
        }

        public bool IsSlimFriendBecameFat()
        {
            return IsSlimFriendGotAFewKilos && !IsPretty;
        }
    }
}