﻿using nunit_testing_framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nunit_testing_homework
{
    class Program
    {
        /*
         1. Object of the class Boy don`t create.
            FIX - code 
            " this.GirlFriend.BoyFriend = this; "
            in main constructor is moved to method SetMyselfAsBoyFriendToMyGirlFriend()
         2. Default constructor not exist for class Boy.
            FIX - is written constructor
         3. In method IsSummerMonth() must be operators ||
            FIX - code
            " Month.July.Equals(BirthdayMonth) && Month.August.Equals(BirthdayMonth); "
            IS CORRECTED ON
            " Month.July.Equals(BirthdayMonth) || Month.August.Equals(BirthdayMonth); "
        4.   " Wealth += amountForSpending; " in SpendSomeMoney method wrong
            FIX - 
            IS CORRECTED ON
            " Wealth -= amountForSpending; "
        5.  Can't spent negative value in SpendSomeMoney(double amountForSpending) method from Boy class
            SpendBoyFriendMoney(double amountForSpending)  method from Girl class
            FIX - in methods is added
            "
             if (amountForSpending < 0)
            {
                throw new ArgumentException(
                                   String.Format(
                                       $"You can't spend negative value$"));
            }
            "
        6.  Object of the class Girl don`t create.
            FIX - code 
            " this.BoyFriend.GirlFriend = this;"
            from main constructor is moved to method SetMyselfAsGirlFriendToMyBoyFriend
         7. IsBoyFriendRich() from class Girl
            " return BoyFriend == null && BoyFriend.IsRich(); " 
            FIX - 
            IS CORRECTED ON
            " return BoyFriend != null && BoyFriend.IsRich(); "
             */
        static void Main(string[] args)
        {
            Boy _boy = new Boy(0, 50000.0, null);
            _boy.SetMyselfAsBoyFriendToMyGirlFriend();
            var _boy2 = new Boy();
            _boy.SpendSomeMoney(500000);
            var _girl = new Girl(true, true, null);

            Console.WriteLine("{0}", _boy.GirlFriend);
            Console.ReadLine();
        }
    }
}
