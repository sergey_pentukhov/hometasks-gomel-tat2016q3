﻿namespace MyTests.Disk.Yandex.Ru
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading;
    using FrameWork;
    using FrameWork.Pages;
    using FrameWork.Services;
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Interactions;

    public class YandexDiskTests : YandexBaseTest
    {
        private const int WAIT_DOWNLOAD_FILE = 10;
        private const int NUMBER_TRYING = 30;

        public const string SECOND_NAME_FILE = "LICENSE.txt";
        public const string FULL_NAME_SECOND_FILE = @"d:\TAT2016\hometask3\MailRu\LICENSE.txt";

        public static By FileLocator = By.XPath("//div[contains(@title, 'Errors.txt')]");
        public static By SecondFileLocator = By.XPath("//div[contains(@title, 'LICENSE.txt')]");
        public static By TrashLocator = By.XPath("//div[contains(@title, 'Корзина')]");

        public byte[] ContentFile { get; private set; }

        public string LocalControlSumm { get; private set; }

        public string DownloadFileControlSumm { get; private set; }

        [Test]
        public void SuccessLogInYandexTest()
        {
            this.LogIn();
        }

        [Test]
        public void UploadFileTest()
        {
            var homePage = this.LogIn();

            PageYandexService.UploadFile(homePage, MailRuSentMailTests.FULL_NAME_FILE);

            this.ContentFile = ReadAllFile(MailRuSentMailTests.FULL_NAME_FILE);
            this.LocalControlSumm = this.GetMD5(this.ContentFile);

            var homePage2 = homePage.MarkFile(FileLocator);
            var homePage3 = homePage2.PushDownload();

            WaitDownloadFile(MailRuSentMailTests.NAME_FILE);

            this.DownloadFileControlSumm = this.GetMD5(ReadAllFile(Settings.DownloadFolder + "\\"
                + MailRuSentMailTests.NAME_FILE));
            Assert.That(this.LocalControlSumm.Equals(this.DownloadFileControlSumm));
        }

        [Order(1)]
        [Test]
        public void UploadFileAndRemoveFileTest()
        {
            var homePage = this.LogIn();
            PageYandexService.UploadFile(homePage, MailRuSentMailTests.FULL_NAME_FILE);

            Assert.That(homePage.GetWebDriver().FindElement(FileLocator).GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));
            new Actions(homePage.GetWebDriver())
                        .DoubleClick(homePage.GetWebDriver().FindElement(TrashLocator))
                        .Perform();

            var homePage2 = homePage.ClickClean();

            homePage2.GetWebDriver().Navigate().Back();
            var homePage3 = homePage2.MoveToTrash(FileLocator);

            new Actions(homePage3.GetWebDriver())
                        .DoubleClick(homePage3.GetWebDriver().FindElement(TrashLocator))
                        .Perform();

            homePage3.GetWebDriver().Navigate().GoToUrl(homePage3.GetWebDriver().Url);
            Assert.That(homePage3.GetWebDriver().FindElement(FileLocator).GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));
        }

        [Order(2)]
        [Test]
        public void RemoveFilePermanentlyTest()
        {
            var homePage = this.LogIn();

            new Actions(homePage.GetWebDriver())
                       .DoubleClick(homePage.GetWebDriver().FindElement(TrashLocator))
                       .Perform();

            homePage.WaitForElementsAppear(FileLocator);
            Assert.That(homePage.GetWebDriver().FindElement(FileLocator).GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));

            var homePage2 = homePage.ClickClean();
            homePage2.GetWebDriver().Navigate().GoToUrl(homePage2.GetWebDriver().Url);

            Assert.Throws<NoSuchElementException>(
                delegate
                {
                    homePage2.GetWebDriver().FindElement(FileLocator);
                });
        }

        [Test]
        public void UploadFileAndRemoveFileAndRestoreFileTest()
        {
            var homePage = this.LogIn();

            PageYandexService.UploadFile(homePage, MailRuSentMailTests.FULL_NAME_FILE);

            Assert.That(homePage.GetWebDriver().FindElement(FileLocator).GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));

            var homePage2 = homePage.MoveToTrash(FileLocator);

            new Actions(homePage2.GetWebDriver())
                        .DoubleClick(homePage2.GetWebDriver().FindElement(TrashLocator))
                        .Perform();
            homePage.WaitForElementsAppear(FileLocator);
            var homePage3 = homePage2.MarkFile(FileLocator);
            var homePage4 = homePage3.ClickRestore();
            homePage4.GetWebDriver().Navigate().Back();
            homePage4.WaitForElementsAppear(FileLocator);
            Assert.That(homePage4.GetWebDriver().FindElement(FileLocator).GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));
        }

        [Test]
        public void RemoveSeveralFilesTest()
        {
            var homePage = this.LogIn();

            PageYandexService.UploadFile(homePage, MailRuSentMailTests.FULL_NAME_FILE);

            Assert.That(homePage.GetWebDriver().FindElement(FileLocator).GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));

            PageYandexService.UploadFile(homePage, FULL_NAME_SECOND_FILE);

            Assert.That(homePage.GetWebDriver().FindElement(SecondFileLocator).GetAttribute("title").Equals(SECOND_NAME_FILE));

            var listUploadedFiles = new List<By>();
            listUploadedFiles.Add(FileLocator);
            listUploadedFiles.Add(SecondFileLocator);

            var homePage2 = homePage.SeveralFilesMoveToTrash(listUploadedFiles);

            homePage2.WaitForElementsDisAppear(listUploadedFiles[0]);

            new Actions(homePage2.GetWebDriver())
                        .DoubleClick(homePage2.GetWebDriver().FindElement(TrashLocator))
                        .Perform();

            foreach (var l in listUploadedFiles)
            {
                homePage.WaitForElementsAppear(l);
                Assert.Pass();
            }
        }

        private static void WaitDownloadFile(string fullNameFile)
        {
            var timeCount = 0;
            bool exit = false;

            while (true)
            {
                var allDownloadsFiles = Directory.GetFiles(Settings.DownloadFolder);
                foreach (var f in allDownloadsFiles)
                {
                    if (f.Contains(Path.GetFileNameWithoutExtension(fullNameFile))
                        && (DateTime.Now - File.GetCreationTime(f) <= TimeSpan.FromSeconds(WAIT_DOWNLOAD_FILE)))
                    {
                        exit = true;
                        break;
                    }
                }

                if (exit)
                {
                    break;
                }

                Thread.Sleep(1000);
                timeCount++;

                if (timeCount == NUMBER_TRYING)
                {
                    throw new FileLoadException("File not loaded!");
                }
            }
        }

        private static byte[] ReadAllFile(string filename)
        {
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                byte[] buff = new byte[file.Length];
                file.Read(buff, 0, (int)file.Length);
                return buff;
            }
        }

        private string GetMD5(byte[] hash)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hashenc = md5.ComputeHash(hash);
            var result = new StringBuilder();
            foreach (var b in hashenc)
            {
                result.Append(b.ToString("x2"));
            }

            return result.ToString();
        }

        private HomeYandexPage LogIn()
        {
            this.loginPage.OpenDefaultURL(Settings.FileJobUrl);
            var homePage = this.loginPage.SignIn(Settings.LoginYandex, Settings.PasswordYandex);
            Assert.That(homePage.GetUserName().Equals(Settings.LoginYandex));
            return homePage;
        }
    }
}
