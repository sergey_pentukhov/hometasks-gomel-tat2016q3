﻿namespace MyTests
{
    using FrameWork;
    using NUnit.Framework;
    using OpenQA.Selenium;

    public class BaseTest
    {
        protected IWebDriver driver;
        protected LoginPage loginPage;

        [SetUp]
        public void FixtureBaseSetup()
        {
            this.driver = Browser.GetBrowser(Settings.ReserveBrowser).Driver;
            this.loginPage = new LoginPage(this.driver);
        }

        [TearDown]
        public void FixtureBaseTearDown()
        {
            Browser.GetBrowser().Close();
        }
    }
}
