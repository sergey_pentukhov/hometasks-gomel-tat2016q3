﻿/// <summary>
/// Page after sign in
/// </summary>
namespace FrameWork
{
    using OpenQA.Selenium;

    public class HomeMailRuPage : Page
    {
        public static By LabelUserNameLocator = By.XPath("//*[@id='PH_user-email']");
        public static By HomeLabelLoginInputLocator = By.XPath("//*[@id='PH_user-email']");
        public static By WriteMailButtonLocator = By.XPath("//a[@data-name='compose']");

        public static By SubjectInFirstRowLocator = By.XPath(
            ".//div[@class='b-datalist__body']/div[1]//div[@class='b-datalist__item__subj']");

        public static By TextInFirstRowLocator = By.XPath(
            ".//div[@class='b-datalist__body']/div[1]/div[1]/a//span[@class='b-datalist__item__subj__snippet']");

        public static By OutboxLocator = By.XPath(".//*[@class='ico ico_folder ico ico_folder_send']");
        public static By InboxLocator = By.XPath(".//a[@class='b-nav__link' and @href='/messages/inbox/']");
        public static By PopupLocator = By.XPath(
            "//button[@class='btn btn_stylish btn_main confirm-ok']/span[text()='Продолжить']");

        public static By DraftLocator = By.XPath(".//*[@class='ico ico_folder ico ico_folder_drafts']");
        public static By TrashLocator = By.XPath("//a[@href='/messages/trash/']");

        public static By InboxReadyLocator = By.XPath(
            ".//a[@href='/messages/inbox/' and @rel='history' and not(@title)]");

        private Element labelUserName = new Element(LabelUserNameLocator);
        private Element homeLabelLoginInput = new Element(HomeLabelLoginInputLocator);
        private Element writeMailButton = new Element(WriteMailButtonLocator);
        private Element subjectInFirstRow = new Element(SubjectInFirstRowLocator);
        private Element textInFirstRow = new Element(TextInFirstRowLocator);
        private Element outbox = new Element(OutboxLocator);
        private Element inbox = new Element(InboxLocator);
        private Element popup = new Element(PopupLocator);
        private Element draft = new Element(DraftLocator);
        private Element trash = new Element(TrashLocator);
        private Element inboxReady = new Element(InboxReadyLocator);

        public HomeMailRuPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public string GetUserName()
        {
            return this.homeLabelLoginInput.GetText();
        }

        public PageForWriteMail ClickForWriteMail()
        {
            this.writeMailButton.Click();
            return new PageForWriteMail(this.GetWebDriver());
        }

        public HomeMailRuPage ClickInbox()
        {
            this.inboxReady.WaitForAppear();
            this.inbox.Click();
            return new HomeMailRuPage(this.GetWebDriver());
        }

        public OutboxPage ClickOutbox()
        {
            this.inboxReady.WaitForAppear();
            this.outbox.Click();
            return new OutboxPage(this.GetWebDriver());
        }

        public DraftPage ClickDraft()
        {
            this.draft.Click();
            return new DraftPage(this.GetWebDriver());
        }

        public TrashPage ClickTrash()
        {
            this.trash.Click();
            return new TrashPage(this.GetWebDriver());
        }

        public HomeMailRuPage ClickPopup()
        {
            this.popup.Click();
            return new HomeMailRuPage(this.GetWebDriver());
        }

        public virtual string GetSubject()
        {
            return this.subjectInFirstRow.GetText().Trim();
        }

        public virtual string GetMailText()
        {
            return this.textInFirstRow.GetText().Trim();
        }
    }
}
