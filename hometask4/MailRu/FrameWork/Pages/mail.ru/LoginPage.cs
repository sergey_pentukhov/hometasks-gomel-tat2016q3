﻿namespace FrameWork
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;

    public class LoginPage : Page
    {
        public static By LoginInputLocator = By.XPath(loginXPath);
        public static By PasswordInputLocator = By.XPath(passwordXPath);
        public static By SubmitInputLocator = By.XPath(submitXPath);

        private const string loginXPath = "//*[@id='mailbox__login']";
        private const string passwordXPath = "//*[@id='mailbox__password']";
        private const string submitXPath = "//*[@id='mailbox__auth__button']";

        private Element userName = new Element(LoginInputLocator);
        private Element password = new Element(PasswordInputLocator);
        private Element submitButton = new Element(SubmitInputLocator);

        public LoginPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public HomeMailRuPage SignIn(string username, string password)
        {
            this.userName.TypeValue(username);
            this.password.TypeValue(password);
            this.submitButton.Click();
            return new HomeMailRuPage(this.GetWebDriver());
        }

        public ErrorMailRuPage ErrorSignIn(string username, string password)
        {
            this.userName.TypeValue(username);
            this.password.TypeValue(password);
            this.submitButton.Click();
            return new ErrorMailRuPage(this.GetWebDriver());
        }
    }
}
