﻿namespace FrameWork
{
    using OpenQA.Selenium;

    public class DraftPage : OutboxPage
    {
        public static By FirstCheckBoxLocator = By.XPath(
            @".//div[@data-cache-key='500001_undefined_false']//div[@class='b-datalist__body']
                    /div[1]/div/a/div/div[@class='js-checkbox b-checkbox']");

        public static By RemoveButtonLocator = By.XPath(
            @".//div[@data-cache-key='500001_undefined_false']//i[@class='ico ico_toolbar ico_toolbar_remove']");

        private Element firstCheckBox = new Element(FirstCheckBoxLocator);
        private Element removeButtonLocator = new Element(RemoveButtonLocator);

        public DraftPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public DraftPage CheckBoxMail()
        {
            this.firstCheckBox.Click();
            return new DraftPage(this.GetWebDriver());
        }

        public DraftPage RemoveMail()
        {
            this.removeButtonLocator.Click();
            return new DraftPage(this.GetWebDriver());
        }
    }
}