﻿namespace FrameWork.Pages
{
    using System.Collections.Generic;
    using FrameWork;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Interactions;
    using OpenQA.Selenium.Support.PageObjects;

    public class HomeYandexPage : Page
    {
        public static By HomeLabelLoginInputLocator = By.XPath(".//*[@class='header__username']");
        public static By UploadFileButtonLocator = By.XPath(".//input[@class='button__attach']");
        public static By CloseLocator = By.XPath("//a[@data-click-action='dialog.close']");
        public static By DownloadButtonLocator = By.XPath(".//*[@data-click-action='resource.download']");
        public static By CleanLocator = By.XPath(".//*[text()[contains(.,'Очистить')]]");
        public static By CleanConfirmLocator = By.XPath(".//html[@id='nb-1']//span[text()='Очистить']");
        public static By PopupReUploadLocator = By.XPath("//div[contains(text(),'Загрузка')]");
        public static By RestoreLocator = By.XPath(".//*[@data-click-action='resource.restore']");
        public static By TrashLocator = By.XPath("//div[contains(@title, 'Корзина')]");
        
        private Element homeLabelLoginInput = new Element(HomeLabelLoginInputLocator);
        private Element uploadFileButton = new Element(UploadFileButtonLocator);
        private Element close = new Element(CloseLocator);
        private Element downloadButton = new Element(DownloadButtonLocator);
        private Element clean = new Element(CleanLocator);
        private Element cleanConfirm = new Element(CleanConfirmLocator);
        private Element popupReUpload = new Element(PopupReUploadLocator);
        private Element restore = new Element(RestoreLocator);
        //private Element trash = new Element(TrashLocator);

        public HomeYandexPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        //[FindsBy(How = How.XPath, Using = "//div[contains(@title, 'Корзина')]")]
        //public IWebElement Trash { get; set; }

        public string GetUserName()
        {
            return this.homeLabelLoginInput.GetText();
        }

        public HomeYandexPage UploadFile(string fullPathFile)
        {
            this.uploadFileButton.TypeValue(fullPathFile);
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage ClickPopupReUpload()
        {
            this.popupReUpload.Click();
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage PushDownload()
        {
            this.uploadFileButton.WaitForAppear();
            this.downloadButton.Click();
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage MarkFile(By fileLocator)
        {
            this.WaitForElementsAppear(fileLocator);
            this.GetWebDriver().FindElement(fileLocator).Click();
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage MoveToTrash(By fileLocator)
        {
            this.WaitForElementsAppear(fileLocator);
            new Actions(this.GetWebDriver())
                           .ClickAndHold(this.GetWebDriver().FindElement(fileLocator))
                           .MoveToElement(this.GetWebDriver().FindElement(TrashLocator))
                           .Release()
                           .Build()
                           .Perform();
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage SeveralFilesMoveToTrash(List<By> listUploadedFiles)
        {
            var action1 = new Actions(this.GetWebDriver())
                           .KeyDown(Keys.LeftControl);
            Actions action2 = action1;
            for (var i = 0; i < listUploadedFiles.Count; i++)
            {
                action2 = action2
                          .Click(this.GetWebDriver().FindElement(listUploadedFiles[i]));
            }

            action2
                   .ClickAndHold(this.GetWebDriver().
                           FindElement(listUploadedFiles[listUploadedFiles.Count - 1]))
                   .KeyUp(Keys.LeftControl)
                   .MoveToElement(this.GetWebDriver().FindElement(TrashLocator))
                   .Release()
                   .Build()
                   .Perform();

            return new HomeYandexPage(this.GetWebDriver());
        }

        public bool IsPopupPresent()
        {
            bool isPopupReUploadPresent;
            try
            {
                this.popupReUpload.WaitForAppear();
                isPopupReUploadPresent = true;
            }
            catch (NoSuchElementException)
            {
                isPopupReUploadPresent = false;
            }

            return isPopupReUploadPresent;
        }

        public HomeYandexPage ClickClean()
        {
            try
            {
                if (this.clean.IsVisible())
                {
                    this.clean.Click();
                    this.WaitForElementsAppear(CleanConfirmLocator);
                    IWebElement element = this.GetWebDriver().FindElement(CleanConfirmLocator);
                    Actions actions = new Actions(this.GetWebDriver());
                    actions.MoveToElement(element).Click().Perform();
                }
            }
            catch (NoSuchElementException)
            {
                return new HomeYandexPage(this.GetWebDriver());
            }

            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage ClickRestore()
        {
            this.WaitForElementsAppear(RestoreLocator);
            IWebElement element = this.GetWebDriver().FindElement(RestoreLocator);
            Actions actions = new Actions(this.GetWebDriver());
            actions.MoveToElement(element).Click().Perform();
            return new HomeYandexPage(this.GetWebDriver());
        }
    }
}