﻿namespace FrameWork.Pages
{
    using OpenQA.Selenium;

    public class YandexLoginPage : Page
    {
        public static By LoginInputLocator = By.XPath(".//*[@id='nb-22']");
        public static By PasswordInputLocator = By.XPath(".//*[@id='nb-23']");
        public static By SubmitInputLocator = By.XPath(".//*[@id='nb-21']");

        private Element loginInput = new Element(LoginInputLocator);
        private Element passwordInput = new Element(PasswordInputLocator);
        private Element submitInput = new Element(SubmitInputLocator);

        public YandexLoginPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public HomeYandexPage SignIn(string username, string password)
        {
            this.loginInput.TypeValue(username);
            this.passwordInput.TypeValue(password);
            this.submitInput.Click();
            return new HomeYandexPage(this.GetWebDriver());
        }
    }
}
