﻿namespace FrameWork.Services
{
    using Pages;

    public static class PageYandexService
    {
        public static void UploadFile(HomeYandexPage page, string fullNameFile)
        {
            page.UploadFile(fullNameFile);
            bool isPopupReUploadPresent = page.IsPopupPresent();

            if (isPopupReUploadPresent)
            {
                page = page.ClickPopupReUpload();
            }

            page.GetWebDriver().Navigate().GoToUrl(page.GetWebDriver().Url);
        }
    }
}
