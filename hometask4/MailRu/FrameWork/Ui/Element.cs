﻿namespace FrameWork
{
    using OpenQA.Selenium;

    public class Element
    {
        public Element(By by)
        {
            this.by = by;
        }

        private By by;

        public By GetBy()
        {
            return by;
        }

        public IWebElement GetWrappedWebElement()
        {
            return Browser.GetBrowser().FindElement(this.by);
        }

        public bool IsPresent()
        {
            return Browser.GetBrowser().IsElementPresent(this.by);
        }

        public bool IsVisible()
        {
            return Browser.GetBrowser().IsElementVisible(this.by);
        }

        public void WaitForAppear()
        {
            Browser.GetBrowser().WaitForElementIsVisible(this.by);
        }

        public string GetText()
        {
            this.WaitForAppear();
            return this.GetWrappedWebElement().Text;
        }

        public void Click()
        {
            this.WaitForAppear();
            this.GetWrappedWebElement().Click();
        }

        public void TypeValue(string value)
        {
            this.WaitForAppear();
            IWebElement element = this.GetWrappedWebElement();
            element.Clear();
            element.SendKeys(value);
        }
    }
}