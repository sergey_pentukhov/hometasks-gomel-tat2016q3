﻿namespace FrameWork
{
    using System;
    using System.Linq;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    public sealed class Browser
    {
        public const int ELEMENT_WAIT_TIMEOUT_SECONDS = 15;
        private static object threadLock = new object();

        private static Browser instance;

        private Browser(string settingBrowser = null)
        {
            this.Driver = WebDriverFactory.GetWebDriver(settingBrowser);
        }

        public IWebDriver Driver { get; private set; }

        public string PageUrl
        {
            get
            {
                return this.Driver.Url;
            }
        }

        public static Browser GetBrowser(string settingBrowser = null)
        {
            lock (threadLock)
            {
                if (instance == null)
                {
                    instance = new Browser(settingBrowser);
                }

                return instance;
            }
        }

        public void Close()
        {
            try
            {
                if (this.Driver != null)
                {
                    this.Driver.Quit();
                }
            }
            finally
            {
                instance = null;
            }
        }


        //public WebDriver getWrappedDriver()
        //{
        //    return Driver;
        //}

        public void Open(string url)
        {
            this.Driver.Navigate().GoToUrl(url);
        }

        public IWebElement FindElement(By by)
        {
            return this.Driver.FindElement(by);
        }

        public bool IsElementPresent(By by)
        {
            return this.Driver.FindElements(by).Count() > 0;
        }

        public bool IsElementVisible(By by)
        {
            if (!this.IsElementPresent(by))
            {
                return false;
            }

            return this.FindElement(by).Displayed;
        }

        public void WaitForElementIsPresent(By by)
        {
            //Log.debug("WaitForElementIsPresent " + by);
            WebDriverWait wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(ELEMENT_WAIT_TIMEOUT_SECONDS));
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(by));
        }

        public void WaitForElementIsVisible(By by)
        {
            //Log.debug("WaitForElementIsVisible " + by);
            WebDriverWait wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(ELEMENT_WAIT_TIMEOUT_SECONDS));
            wait.Until(ExpectedConditions.ElementIsVisible(by));
        }

        public void WaitForElementIsNotVisible(By by)
        {
            //Log.debug("WaitForElementIsNotVisible " + by);
            WebDriverWait wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(ELEMENT_WAIT_TIMEOUT_SECONDS));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(by));
        }

    }
}
