﻿using System;
using System.Diagnostics;
using System.Configuration;

namespace ConsoleRunner
{
    public class Runner
    {
        private static string pathToSelenium = AppDomain.CurrentDomain.BaseDirectory 
                        + ConfigurationManager.AppSettings["SeleniumServerFile"];

        static void Main(string[] args)
        {
            Process.Start(pathToSelenium);
        }
    }
}
