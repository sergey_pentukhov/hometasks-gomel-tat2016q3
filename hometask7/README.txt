Own cli option:

        [Option('b', "browser", Required = true,
        HelpText = "Input file to be processed.")]
        public string Browser { get; set; }

Examples (my):
MailRu.exe -b Chrome 					// my common assembly call like this because first task was MailRu
MailRu.dll -b Firefox					// enum TypeBrowser {Firefox,Chrome}
MailRu.dll --browser Firefox



host and port use ONLY together:

        [Option('h', "host", Required = false,
        HelpText = "Remote host of web driver.")]
        public string Host { get; set; }


MailRu.exe -h localhost   // ERROR 				
MailRu.dll --host 127.0.0.1   //ERROR

        [Option('p', "port", Required = false,
        HelpText = "Remote port of web driver.")]
        public string Port { get; set; }

MailRu.exe --port 4444   // ERROR 				
MailRu.dll -p 1298   //ERROR

Examples (my):
MailRu.exe -h localhost --port 4444 
MailRu.dll --host 127.0.0.1 -p 1298
 



        [Option('s', "suite", Required = false,
        HelpText = "Test-suite in App.config file.")]
        public string Suite { get; set; }

Examples (my):							// in App.config file is defined 2 Test-suite :
MailRu.exe -s TestSuiteYandex					// <add key="TestSuiteMailRu" value="MyTests.MailRuSentMailTests" />
MailRu.dll --suite TestSuiteMailRu				// <add key="TestSuiteYandex" value="MyTests.Disk.Yandex.Ru.YandexDiskTests" />

        [Option('m', "multi", Required = false,
        HelpText = "Multi running (parallel run test fixture)")]
        public string Multi { get; set; }

Examples (my):							// in App.config file is defined 2 Test-suite :
MailRu.exe -m 2					
MailRu.dll --multi 2 						// 2 - amount parallel test fixture , then amount is sent like a --workers parameter

        [Option('v', "verbose", DefaultValue = true,
        HelpText = "Prints all messages to standard output.")]