﻿namespace MyTests
{
    using FrameWork;
    using FrameWork.Logging;
    using NUnit.Framework;
    using NUnitLite.Tests;
    using OpenQA.Selenium;
    using RelevantCodes.ExtentReports;

    public class BaseTest : ExtentReporter
    {
        protected IWebDriver driver;
        protected MyEventListener myEventListener;

        public BaseTest()
        {
            this.myEventListener = new MyEventListener();
        }

        [SetUp]
        public void FixtureBaseSetup()
        {
            string logMessage = "----- Test start: " + TestContext.CurrentContext.Test.MethodName + "-----";

            this.test = this.extent.StartTest(logMessage);
            this.test.Log(LogStatus.Info, "Start");
            MyNLogger.Info(logMessage);

            this.driver = Browser.GetBrowser(ConfigStart.Config).Driver;
            this.myEventListener.OnStart("Test start..");
        }

        [TearDown]
        public void FixtureBaseTearDown()
        {
            this.myEventListener.OnTestEvent("Browser close");
        }
    }
}
