﻿namespace NUnitLite.Tests
{
    using CommandLine;
    using CommandLine.Text;

    public class Options
    {
        [Option('b', "browser", Required = true, HelpText = "Input file to be processed.")]
        public string Browser { get; set; }

        [Option('h', "host", Required = false, HelpText = "Remote host of web driver.")]
        public string Host { get; set; }

        [Option('p', "port", Required = false, HelpText = "Remote port of web driver.")]
        public string Port { get; set; }

        [Option('s', "suite", Required = false, HelpText = "Test-suite in App.config file.")]
        public string Suite { get; set; }

        [Option('m', "multi", Required = false, HelpText = "Multi running (parallel run test fixture)")]
        public string Multi { get; set; }

        [Option('v', "verbose", DefaultValue = true, HelpText = "Prints all messages to standard output.")]
        public bool Verbose { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(
                this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}
