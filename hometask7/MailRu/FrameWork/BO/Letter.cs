﻿namespace FrameWork.BO
{
    public class Letter
    {
        private string to;
        private string subject;
        private string letterText;

        public Letter(string to = null, string topic = null, string textOfMail = null)
        {
            this.to = to;
            this.subject = topic;
            this.letterText = textOfMail;
        }

        public string To
        {
            get { return this.to; }
            set { this.to = value; }
        }

        public string Topic
        {
            get { return this.subject; }
            set { this.subject = value; }
        }

        public string TextOfMail
        {
            get { return this.letterText; }
            set { this.letterText = value; }
        }
    }
}
