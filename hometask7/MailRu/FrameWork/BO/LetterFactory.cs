﻿namespace FrameWork.BO
{
    using System;
    using FrameWork.Services;

    public enum LetterTypes
    {
        WithoutSubjectAndBody,
        WithoutAdress,
        Full
    }

    public static class LetterFactory
    {
        public static Letter CreateMail(LetterTypes enumType)
        {
            Random rnd = new Random();
            var to = System.Configuration.ConfigurationManager.AppSettings["Email"];

            switch (enumType)
            {
                case LetterTypes.WithoutAdress:
                    return new Letter();

                case LetterTypes.WithoutSubjectAndBody:
                    return new Letter(to);

                case LetterTypes.Full:
                    return new Letter(
                        to,
                        RandomString.GetRandomString(rnd.Next(1, 15)),
                        RandomString.GetRandomString(rnd.Next(1, 15)));

                default:
                    return new Letter(
                        to,
                        RandomString.GetRandomString(rnd.Next(1, 15)),
                        RandomString.GetRandomString(rnd.Next(1, 15)));
            }
        }
    }
}
