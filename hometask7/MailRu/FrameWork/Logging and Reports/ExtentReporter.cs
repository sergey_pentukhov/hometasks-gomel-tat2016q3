﻿namespace FrameWork.Logging
{
    using System;
    using NUnit.Framework;
    using NUnit.Framework.Interfaces;
    using RelevantCodes.ExtentReports;

    public class ExtentReporter
    {
        protected ExtentReports extent;
        protected ExtentTest test;

        [OneTimeSetUp]
        public void InitReport()
        {
            this.extent = ExtentManager.Instance;
            this.StartReport();
        }

        [TearDown]
        public void GetReportResult()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stacktrace = string.IsNullOrEmpty(TestContext.CurrentContext.Result.StackTrace)
                    ? string.Empty
                    : string.Format("<pre>{0}</pre>", TestContext.CurrentContext.Result.StackTrace);
            var errorMessage = TestContext.CurrentContext.Result.Message;

            LogStatus logstatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = LogStatus.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logstatus = LogStatus.Warning;
                    break;
                case TestStatus.Skipped:
                    logstatus = LogStatus.Skip;
                    break;
                default:
                    logstatus = LogStatus.Pass;
                    break;
            }

            var logMessage = "----- Test ended with " + logstatus + stacktrace + errorMessage + " -----";
            this.test.Log(logstatus, logMessage);

            MyNLogger.Info(logMessage);
            this.extent.EndTest(this.test);
            this.extent.Flush();
        }

        private void StartReport()
        {
            var path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../");
            string nameFile = "extent-config.xml";
            this.extent.AddSystemInfo("Host Name", "localhost")
                .AddSystemInfo("Environment", "QA")
                .AddSystemInfo("User Name", "S. Pentsiukhou");
            this.extent.LoadConfig(path + nameFile);
        }
    }
}
