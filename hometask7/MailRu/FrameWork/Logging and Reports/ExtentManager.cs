﻿namespace FrameWork.Logging
{
    using RelevantCodes.ExtentReports;

    internal class ExtentManager
    {
        private static readonly ExtentReports instance =
            new ExtentReports("../../../ExtentReport.html", true, DisplayOrder.NewestFirst);

        static ExtentManager()
        {
        }

        private ExtentManager()
        {
        }

        public static ExtentReports Instance
        {
            get
            {
                return instance;
            }
        }
    }
}
