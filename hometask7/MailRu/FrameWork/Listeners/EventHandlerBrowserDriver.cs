﻿namespace FrameWork.Listeners
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.Events;

    public class EventHandlerBrowserDriver : EventFiringWebDriver
    {
        public EventHandlerBrowserDriver(IWebDriver parentDriver)
            : base(parentDriver)
        {
        }

        protected override void OnElementClicking(WebElementEventArgs e)
        {
            MyNLogger.Debug(string.Format(
                " ~~~~~ Element {0} with text {1} has clicked ~~~~~ ",
                e.Element.TagName,
                e.Element.Text));

            base.OnElementClicking(e);
        }

        protected override void OnElementClicked(WebElementEventArgs e)
        {
            base.OnElementClicked(e);
        }

        protected override void OnElementValueChanged(WebElementEventArgs e)
        {
            MyNLogger.Debug(string.Format(
                " ~~~~~ Value of element {0} was changed ~~~~~ ", e.Element));

            base.OnElementValueChanged(e);
        }

        protected override void OnException(WebDriverExceptionEventArgs e)
        {
            MyNLogger.Warn(
                string.Format(
                " ~~~~~ Warning {0}. ~~~~~ ", e.ThrownException.Message));

            base.OnException(e);
        }

        protected override void OnFindElementCompleted(FindElementEventArgs e)
        {
            base.OnFindElementCompleted(e);

            MyNLogger.Info(string.Format(
                " ~~~~~ Searching method was found element {0} sucsessful! ~~~~~ ", e.FindMethod));
        }

        protected override void OnNavigating(WebDriverNavigationEventArgs e)
        {
            base.OnNavigating(e);
            MyNLogger.Info(string.Format(" ~~~~~ Navigating .. ~~~~~ "));
        }

        protected override void OnNavigatingBack(WebDriverNavigationEventArgs e)
        {
            base.OnNavigatingBack(e);
            MyNLogger.Info(string.Format(" ~~~~~ Navigating back.. ~~~~~ "));
        }

        protected override void OnNavigated(WebDriverNavigationEventArgs e)
        {
            base.OnNavigated(e);
            MyNLogger.Info(string.Format(" ~~~~~ Was opened {0} ~~~~~ ", e.Url));
        }
    }
}
