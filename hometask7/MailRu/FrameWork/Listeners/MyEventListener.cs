﻿namespace FrameWork
{
    using System.Diagnostics;
    using NUnit.Engine;
    using NUnit.Engine.Extensibility;

    [Extension(EngineVersion = "3.4")]
    public class MyEventListener : ITestEventListener
    {
        /// <summary>
        /// Handle a progress report or other event.
        /// </summary>
        /// <param name="report">An XML progress report.</param>
        public void OnTestEvent(string report)
        {
            Debug.WriteLine(string.Format("++++++++++++++ {0} +++++++++++", report));
            Browser.GetBrowser().Close();
        }

        public void OnStart(string report)
        {
            MyNLogger.Info(string.Format("++++++++++++++ {0} +++++++++++", report));
        }
    }
}
