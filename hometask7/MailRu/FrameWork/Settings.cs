﻿namespace FrameWork
{
    using System.Configuration;

    public class Settings
    {
        public static string SiteUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["DefaultUrl"];
            }
        }

        public static string Email
        {
            get
            {
                return ConfigurationManager.AppSettings["Email"];
            }
        }

        public static string SeleniumServerFileName
        {
            get
            {
                return ConfigurationManager.AppSettings["SeleniumServerFileName"];
            }
        }

        public static string FileJobUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["FileJobUrl"];
            }
        }

        public static string LoginMailRu
        {
            get
            {
                return ConfigurationManager.AppSettings["LoginMailRu"];
            }
        }

        public static string LoginYandex
        {
            get
            {
                return ConfigurationManager.AppSettings["LoginYandex"];
            }
        }

        public static string PasswordYandex
        {
            get
            {
                return ConfigurationManager.AppSettings["PasswordYandex"];
            }
        }

        public static string PasswordMailRu
        {
            get
            {
                return ConfigurationManager.AppSettings["PasswordMailRu"];
            }
        }

        public static string BaseBrowser
        {
            get
            {
                return ConfigurationManager.AppSettings["BaseBrowser"];
            }
        }

        public static string ReserveBrowser
        {
            get
            {
                return ConfigurationManager.AppSettings["ReserveBrowser"];
            }
        }

        public static string DownloadFolder
        {
            get
            {
                return ConfigurationManager.AppSettings["DownloadFolder"];
            }
        }

        public static string Locator
        {
            // Locator (CSS or XPath)
            get
            {
                return ConfigurationManager.AppSettings["Locator"];
            }
        }

        public static string TestSuiteMailRu
        {
            get
            {
                return ConfigurationManager.AppSettings["TestSuiteMailRu"];
            }
        }

        public static string TestSuiteYandex
        {
            get
            {
                return ConfigurationManager.AppSettings["TestSuiteYandex"];
            }
        }
    }
}
