﻿namespace FrameWork
{
    using System;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Interactions;

    public class Element : IWebElement
    {
        private By by;

        public Element(By by)
        {
            this.by = by;
        }

        public string TagName
        {
            get
            {
                return this.FindElement(this.by).TagName;
            }
        }

        public string Text
        {
            get
            {
                return this.FindElement(this.by).Text;
            }
        }

        public bool Enabled
        {
            get
            {
                return this.FindElement(this.by).Enabled;
            }
        }

        public bool Selected
        {
            get
            {
                return this.FindElement(this.by).Selected;
            }
        }

        public Point Location
        {
            get
            {
                return this.FindElement(this.by).Location;
            }
        }

        public Size Size
        {
            get
            {
                return this.FindElement(this.by).Size;
            }
        }

        public bool Displayed
        {
            get
            {
                return this.FindElement(this.by).Displayed;
            }
        }

        public By GetBy()
        {
            return this.by;
        }

        public IWebElement GetWrappedWebElement()
        {
            return Browser.GetBrowser().FindElement(this.by);
        }

        public bool IsPresent()
        {
            return Browser.GetBrowser().IsElementPresent(this.by);
        }

        public bool IsVisible()
        {
            return Browser.GetBrowser().IsElementVisible(this.by);
        }

        public void WaitForAppear()
        {
            Browser.GetBrowser().WaitForElementIsVisible(this.by);
        }

        public string GetText()
        {
            this.WaitForAppear();
            MyNLogger.Debug("Get text from element <" + this.GetWrappedWebElement().Text + ">");
            return this.GetWrappedWebElement().Text;
        }

        public void Click()
        {
            this.WaitForAppear();
            this.GetWrappedWebElement().Click();
            MyNLogger.Info("Click.");
        }

        public void TypeValue(string value)
        {
            IWebElement element = this.GetWrappedWebElement();
            try
            {
                element.Clear();
                element.SendKeys(value);
                MyNLogger.Debug("Type value <" + value + ">");
            }
            catch (Exception ex)
            {
                MyNLogger.Error("Stale element for SendKeys!", ex);
            }
        }

        public void DoubleClick()
        {
            IWebElement element = this.GetWrappedWebElement();
            Actions action = new Actions(Browser.GetBrowser().Driver);
            action.DoubleClick(element);
            action.Build().Perform();
            MyNLogger.Info("DoubleClick on " + element);
        }

        public void Clear()
        {
            try
            {
                IWebElement element = this.FindElement(this.by);
                element.Clear();
                MyNLogger.Info("Clear content on " + element.TagName + "with text" + element.Text);
            }
            catch (StaleElementReferenceException ex)
            {
                MyNLogger.Error("Stale element for Clear!", ex);
            }
        }

        public void SendKeys(string text)
        {
            this.FindElement(this.by).SendKeys(text);
        }

        public void Submit()
        {
            try
            {
                IWebElement element = this.FindElement(this.by);
                element.Submit();
                MyNLogger.Info("Submit element " + element.TagName + "with text" + element.Text);
            }
            catch (StaleElementReferenceException ex)
            {
                MyNLogger.Error("Stale element for Submit!", ex);
            }
        }

        public string GetAttribute(string attributeName)
        {
            string valueOfAttribute = null;
            try
            {
                IWebElement element = this.FindElement(this.by);
                valueOfAttribute = element.GetAttribute(attributeName);
            }
            catch (StaleElementReferenceException ex)
            {
                MyNLogger.Error("Stale element for GetAttribute!", ex);
            }

            return valueOfAttribute;
        }

        public string GetCssValue(string propertyName)
        {
            return this.FindElement(this.by).GetCssValue(propertyName);
        }

        public IWebElement FindElement(By by)
        {
            return Browser.GetBrowser().FindElement(by);
        }

        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            return Browser.GetBrowser().FindElements(by);
        }
    }
}