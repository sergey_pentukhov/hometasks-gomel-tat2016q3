﻿namespace FrameWork.Pages
{
    using System.Collections.Generic;
    using FrameWork;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Interactions;

    public class HomeYandexPage : Page
    {
        public static By HomeLabelLoginInputLocator = By.XPath(".//*[@class='header__username']");
        public static By CloseLocator = By.XPath("//a[@data-click-action='dialog.close']");
        public static By CleanLocator = By.XPath(".//*[text()[contains(.,'Очистить')]]");
        public static By CleanConfirmLocator = By.XPath(".//html[@id='nb-1']//span[text()='Очистить']");
        public static By RestoreLocator = By.XPath(".//*[@data-click-action='resource.restore']");
        public static By TrashLocator = By.XPath("//div[contains(@title, 'Корзина')]");

        private Element homeLabelLoginInput = new Element(HomeLabelLoginInputLocator);
        private Element close = new Element(CloseLocator);
        private Element clean = new Element(CleanLocator);
        private Element cleanConfirm = new Element(CleanConfirmLocator);
        private Element restore = new Element(RestoreLocator);
        private Element trash = new Element(TrashLocator);

        public HomeYandexPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public string GetUserName()
        {
            return this.homeLabelLoginInput.GetText();
        }

        public HomeYandexPage MarkFile(By fileLocator)
        {
            Browser.GetBrowser().WaitForElementIsVisible(fileLocator);
            this.GetWebDriver().FindElement(fileLocator).Click();
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage GoToTrash()
        {
            this.trash.DoubleClick();
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage MoveToTrash(By fileLocator)
        {
            Browser.GetBrowser().WaitForElementIsVisible(fileLocator);
            Browser.GetBrowser().DragAndDrop(fileLocator, TrashLocator);
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage SeveralFilesMoveToTrash(List<By> listUploadedFiles)
        {
            Browser.GetBrowser().CtrlClickElementsAndMoveTo(TrashLocator, listUploadedFiles);
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage ClickClean()
        {
            try
            {
                if (this.clean.IsVisible())
                {
                    this.clean.Click();
                    this.cleanConfirm.WaitForAppear();
                    IWebElement element = this.GetWebDriver().FindElement(CleanConfirmLocator);
                    Actions actions = new Actions(this.GetWebDriver());
                    actions.MoveToElement(element).Click().Perform();
                }
            }
            catch (NoSuchElementException)
            {
                return new HomeYandexPage(this.GetWebDriver());
            }

            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage ClickRestore()
        {
            this.restore.WaitForAppear();
            IWebElement element = this.GetWebDriver().FindElement(RestoreLocator);
            Actions actions = new Actions(this.GetWebDriver());
            actions.MoveToElement(element).Click().Perform();
            return new HomeYandexPage(this.GetWebDriver());
        }
    }
}