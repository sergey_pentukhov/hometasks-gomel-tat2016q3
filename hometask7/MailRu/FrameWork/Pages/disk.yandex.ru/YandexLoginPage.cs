﻿namespace FrameWork.Pages
{
    using System.Configuration;
    using BO;
    using OpenQA.Selenium;

    public class YandexLoginPage : Page, ILoginPage
    {
        public static By LoginInputLocator = By.XPath(".//*[@id='nb-22']");
        public static By PasswordInputLocator = By.XPath(".//*[@id='nb-23']");
        public static By SubmitInputLocator = By.XPath(".//*[@id='nb-21']");

        private Element userName = new Element(LoginInputLocator);
        private Element password = new Element(PasswordInputLocator);
        private Element submitButton = new Element(SubmitInputLocator);

        public YandexLoginPage(IWebDriver webdriver)
            : base(webdriver)
        {
            this.StartUrl = ConfigurationManager.AppSettings["FileJobUrl"];
        }

        public string StartUrl { get; set; }

        public void SignIn(Account account)
        {
            this.userName.TypeValue(account.Login);
            this.password.TypeValue(account.Password);
            this.submitButton.Click();
        }

        public void ErrorSignIn(Account account)
        {
            this.userName.TypeValue(account.Login);
            this.password.TypeValue(account.Password);
            this.submitButton.Click();
        }
    }
}
