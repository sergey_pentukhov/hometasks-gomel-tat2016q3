﻿namespace FrameWork.Pages
{
    using FrameWork.BO;

    public interface ILoginPage
    {
        string StartUrl { get; set; }

        void ErrorSignIn(Account account);

        void SignIn(Account account);

        void OpenDefaultURL(string url);
    }
}