﻿namespace MyTests.Disk.Yandex.Ru
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading;
    using FrameWork;
    using FrameWork.Pages;
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Interactions;

    public class YandexDiskTests : YandexBaseTest
    {
        private const int WAIT_DOWNLOAD_FILE = 10;
        private const int NUMBER_TRYING = 30;

        public byte[] ContentFile { get; private set; }

        public string LocalControlSumm { get; private set; }

        public string DownloadFileControlSumm { get; private set; }

        [Test]
        public void SuccessLogInYandexTest()
        {
            this.LogIn();
        }

        [Test]
        public void UploadFileTest()
        {
            var homePage = this.LogIn();
            var homePage2 = homePage.UploadFile(MailRuSentMailTests.FULL_NAME_FILE);

            this.ContentFile = ReadAllFile(MailRuSentMailTests.FULL_NAME_FILE);
            this.LocalControlSumm = this.GetMD5(this.ContentFile);

            bool isPopupReUploadPresent = homePage2.IsPopupPresent();

            if (isPopupReUploadPresent)
            {
                var homePage3 = homePage2.ClickPopupReUpload();
                homePage3.GetWebDriver().Navigate().GoToUrl(homePage2.GetWebDriver().Url);
            }

            var homePage4 = homePage2.MarkFile();
            var homePage5 = homePage4.PushDownload();
            WaitDownloadFile();

            this.DownloadFileControlSumm = this.GetMD5(ReadAllFile(Settings.DownloadFolder + "\\"
                + MailRuSentMailTests.NAME_FILE));
            Assert.That(this.LocalControlSumm.Equals(this.DownloadFileControlSumm));
        }

        [Order(1)]
        [Test]
        public void UploadFileAndRemoveFileTest()
        {
            var homePage = this.LogIn();
            var homePage2 = homePage.UploadFile(MailRuSentMailTests.FULL_NAME_FILE);

            bool isPopupReUploadPresent = homePage2.IsPopupPresent();

            if (isPopupReUploadPresent)
            {
                var homePage3 = homePage2.ClickPopupReUpload();
                homePage3.GetWebDriver().Navigate().GoToUrl(homePage2.GetWebDriver().Url);
            }

            Assert.That(homePage2.MyTxtFile.GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));
            new Actions(homePage2.GetWebDriver())
                        .DoubleClick(homePage2.Trash)
                        .Perform();

            var homePage4 = homePage2.ClickClean();

            homePage4.GetWebDriver().Navigate().Back();
            var homePage5 = homePage4.MoveToTrash();

            new Actions(homePage5.GetWebDriver())
                        .DoubleClick(homePage5.Trash)
                        .Perform();

            homePage5.GetWebDriver().Navigate().GoToUrl(homePage5.GetWebDriver().Url);
            Assert.That(homePage5.MyTxtFile.GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));
        }

        [Order(2)]
        [Test]
        public void RemoveFilePermanentlyTest()
        {
            var homePage = this.LogIn();

            new Actions(homePage.GetWebDriver())
                       .DoubleClick(homePage.Trash)
                       .Perform();

            homePage.WaitForElementsAppear(HomeYandexPage.FileLocator);
            Assert.That(homePage.MyTxtFile.GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));

            var homePage2 = homePage.ClickClean();
            homePage2.GetWebDriver().Navigate().GoToUrl(homePage2.GetWebDriver().Url);

            Assert.Throws<NoSuchElementException>(
                delegate
                {
                    homePage2.GetWebDriver().FindElement(HomeYandexPage.FileLocator);
                });
        }

        [Test]
        public void UploadFileAndRemoveFileAndRestoreFileTest()
        {
            var homePage = this.LogIn();
            var homePage2 = homePage.UploadFile(MailRuSentMailTests.FULL_NAME_FILE);

            bool isPopupReUploadPresent = homePage2.IsPopupPresent();

            if (isPopupReUploadPresent)
            {
                var homePage3 = homePage2.ClickPopupReUpload();
                homePage3.GetWebDriver().Navigate().GoToUrl(homePage2.GetWebDriver().Url);
            }

            Assert.That(homePage2.MyTxtFile.GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));

            var homePage5 = homePage2.MoveToTrash();

            new Actions(homePage5.GetWebDriver())
                        .DoubleClick(homePage5.Trash)
                        .Perform();
            homePage.WaitForElementsAppear(HomeYandexPage.FileLocator);
            var homePage6 = homePage5.MarkFile();
            var homePage7 = homePage6.ClickRestore();
            homePage7.GetWebDriver().Navigate().Back();
            homePage7.WaitForElementsAppear(HomeYandexPage.FileLocator);
            Assert.That(homePage7.MyTxtFile.GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));
        }

        private static void WaitDownloadFile()
        {
            var timeCount = 0;
            bool exit = false;

            while (true)
            {
                var allDownloadsFiles = Directory.GetFiles(Settings.DownloadFolder);
                foreach (var f in allDownloadsFiles)
                {
                    if (f.Contains(Path.GetFileNameWithoutExtension(MailRuSentMailTests.NAME_FILE))
                        && (DateTime.Now - File.GetCreationTime(f) <= TimeSpan.FromSeconds(WAIT_DOWNLOAD_FILE)))
                    {
                        exit = true;
                        break;
                    }
                }

                if (exit)
                {
                    break;
                }

                Thread.Sleep(1000);
                timeCount++;

                if (timeCount == NUMBER_TRYING)
                {
                    throw new FileLoadException("File not loaded!");
                }
            }
        }

        private static byte[] ReadAllFile(string filename)
        {
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                byte[] buff = new byte[file.Length];
                file.Read(buff, 0, (int)file.Length);
                return buff;
            }
        }

        private string GetMD5(byte[] hash)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hashenc = md5.ComputeHash(hash);
            var result = new StringBuilder();
            foreach (var b in hashenc)
            {
                result.Append(b.ToString("x2"));
            }

            return result.ToString();
        }

        private HomeYandexPage LogIn()
        {
            this.loginPage.OpenDefaultURL(Settings.FileJobUrl);
            var homePage = this.loginPage.SignIn(Settings.LoginYandex, Settings.PasswordYandex);
            Assert.That(homePage.GetUserName().Equals(Settings.LoginYandex));
            return homePage;
        }
    }
}
