﻿namespace MyTests
{
    using FrameWork;
    using FrameWork.Pages;
    using NUnit.Framework;
    using OpenQA.Selenium;

    public class YandexBaseTest
    {
        protected IWebDriver driver;
        protected YandexLoginPage loginPage;

        [SetUp]
        public void FixtureBaseSetup()
        {
            this.driver = SomeBrowser.GetBrowser(Settings.BaseBrowser).Driver;
            this.loginPage = new YandexLoginPage(this.driver);
        }

        [TearDown]
        public void FixtureBaseTearDown()
        {
            SomeBrowser.GetBrowser().Close();
        }
    }
}
