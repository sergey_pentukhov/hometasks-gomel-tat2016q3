﻿namespace MailRu
{
    using FrameWork;
    using NUnit.Framework;
    using System;

    [TestFixture]
    public class MailRuSentMailTests : BaseTest
    {
        public const string INCORRECT_PASSWORD = "dfdfhdgjh";
        public const string INVALID_CREDENTIALS_MESSAGE_PAGE = "Неверное имя пользователя или пароль";
        public const string TEXT_TO_BE_SENT = "Hello";
        public const string SUBJECT = "Hello";
        public const string EMAIL = "1309burbolka@mail.ru";
        public const string TEXT_ALERT = "Не указан адрес";

        [Test]
        public void SuccessLogInMailRuTest()
        {
            this.LogIn();
        }

        [Test]
        public void FailLogInMailRuTest()
        {
            this.loginPage.OpenDefaultURL();
            var errorPage = this.loginPage.ErrorSignIn(Settings.Login, INCORRECT_PASSWORD);
            Assert.That(errorPage.GetErrorMessage().Contains(INVALID_CREDENTIALS_MESSAGE_PAGE));
        }

        [Test]
        public void SentMailFromMailRuCheckInboxTest()
        {
            var homePage = this.LogIn();
            var homePage2 = homePage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(SUBJECT)
                .TypeBody(TEXT_TO_BE_SENT).Send();
            homePage2.ClickInbox();
            Assert.That(homePage2.GetSubject().Contains(SUBJECT));
            Assert.That(homePage2.GetMailText().Contains(TEXT_TO_BE_SENT));
        }

        [Test]
        public void SentMailFromMailRuCheckOutboxTest()
        {
            var homePage = this.LogIn();
            var homePage2 = homePage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(SUBJECT)
                .TypeBody(TEXT_TO_BE_SENT).Send();
            var sendPage = homePage2.ClickOutbox();
            Assert.That(sendPage.GetSubject().Contains(SUBJECT));
            Assert.That(sendPage.GetMailText().Contains(TEXT_TO_BE_SENT));
        }

        [Test]
        public void SentMailFromMailRuEmptyEmailTest()
        {
            var homePage = this.LogIn();
            var homePage2 = homePage.ClickForWriteMail().TypeTo(string.Empty).TypeSubject(SUBJECT)
                .TypeBody(TEXT_TO_BE_SENT).Send();
            Assert.That(homePage2.GetWebDriver().SwitchTo().Alert().Text.Contains(TEXT_ALERT));
            homePage2.GetWebDriver().SwitchTo().Alert().Dismiss();
        }

        private HomeMailRuPage LogIn()
        {
            this.loginPage.OpenDefaultURL();
            var homePage = this.loginPage.SignIn(Settings.Login, Settings.Password);
            Assert.That(homePage.GetUserName().Contains(Settings.Login));
            return homePage;
        }
    }
}
