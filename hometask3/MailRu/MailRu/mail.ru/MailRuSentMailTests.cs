﻿namespace MyTests
{
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using FrameWork;
    using NUnit.Framework;
    using OpenQA.Selenium;

    [TestFixture]
    public class MailRuSentMailTests : BaseTest
    {
        public const string INCORRECT_PASSWORD = "dfdfhdgjh";
        public const string INVALID_CREDENTIALS_MESSAGE_PAGE = "Неверное имя пользователя или пароль";
        public const string TEXT_TO_BE_SENT = "Hello";
        public const string SUBJECT = "Hello";
        public const string EMAIL = "1309burbolka@mail.ru";
        public const string TEXT_ALERT = "Не указан адрес";
        public const string TEXT_NO_SUBJECT = "<Без темы>";
        public const string FULL_NAME_FILE = @"d:\TAT2016\hometask3\MailRu\Errors.txt";
        public const string NAME_FILE = "Errors.txt";

        [Test]
        public void SuccessLogInMailRuTest()
        {
            this.LogIn();
        }

        [Test]
        public void FailLogInMailRuTest()
        {
            this.loginPage.OpenDefaultURL(Settings.SiteUrl);
            var errorPage = this.loginPage.ErrorSignIn(Settings.Login, INCORRECT_PASSWORD);
            Assert.That(errorPage.GetErrorMessage().Contains(INVALID_CREDENTIALS_MESSAGE_PAGE));
        }

        [Test]
        public void SentMailFromMailRuCheckInboxTest()
        {
            var homePage = this.LogIn();
            var homePage2 = homePage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(SUBJECT)
                .TypeBodyWithClear(TEXT_TO_BE_SENT).Send();
            homePage2.ClickInbox();
            Assert.That(homePage2.GetSubject().Contains(SUBJECT));
            Assert.That(homePage2.GetMailText().Contains(TEXT_TO_BE_SENT));
        }

        [Test]
        public void SentMailFromMailRuCheckOutboxTest()
        {
            var homePage = this.LogIn();
            var homePage2 = homePage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(SUBJECT)
                .TypeBodyWithClear(TEXT_TO_BE_SENT).Send();
            var sendPage = homePage2.ClickOutbox();
            Assert.That(sendPage.GetSubject().Contains(SUBJECT));
            Assert.That(sendPage.GetMailText().Contains(TEXT_TO_BE_SENT));
        }

        [Test]
        public void SentMailFromMailRuEmptyAdressEmailTest()
        {
            var homePage = this.LogIn();
            var homePage2 = homePage.ClickForWriteMail().TypeTo(string.Empty).TypeSubject(SUBJECT)
                .TypeBody(TEXT_TO_BE_SENT).Send();
            if (homePage2.FlagAlert)
            {
                Assert.That(homePage2.TextAlert.Contains(TEXT_ALERT));
            }
            else
            {
                Assert.Fail("Alert isn`t presented");
            }
        }

        [Test]
        public void SentMailFromMailRuEmptyEmailCheckInboxTest()
        {
            var homePage = this.LogIn();
            var homePage2 = homePage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(string.Empty)
                .TypeBodyWithClear(string.Empty).Send();
            var homePage3 = homePage2.ClickPopup();
            var homePage4 = homePage3.ClickInbox();
            Assert.That(homePage4.GetSubject().Equals(TEXT_NO_SUBJECT));
            Assert.IsEmpty(homePage4.GetMailText());
        }

        [Test]
        public void SentMailFromMailRuEmptyEmailCheckOutboxTest()
        {
            var homePage = this.LogIn();
            var homePage2 = homePage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(string.Empty)
                .TypeBodyWithClear(string.Empty).Send();
            var homePage3 = homePage2.ClickPopup();
            var homePage4 = homePage3.ClickOutbox();
            Assert.That(homePage4.GetSubject().Equals(TEXT_NO_SUBJECT));
            Assert.IsEmpty(homePage4.GetMailText());
        }

        [Test]
        public void SentMailFromMailRuSaveDraftMailTest()
        {
            var homePage = this.LogIn();
            var draftPage = homePage.ClickDraft();
            var homePage2 = draftPage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(SUBJECT)
                .TypeBodyWithClear(TEXT_TO_BE_SENT).Save();
            var draftPage2 = homePage2.ClickDraft();

            Assert.That(draftPage2.GetSubject().Contains(SUBJECT));
            Assert.That(draftPage2.GetMailText().Contains(TEXT_TO_BE_SENT));

            var dataId = draftPage2.GetWebDriver().FindElement(DraftPage.FirstCheckBoxLocator)
                    .GetAttribute("data-id");
            var draftPage3 = draftPage2.CheckBoxMail();
            var draftPage4 = draftPage3.RemoveMail();
            var trashPage = draftPage4.ClickTrash();

            Assert.That(trashPage.GetSubject().Contains(SUBJECT));
            Assert.That(trashPage.GetMailText().Contains(TEXT_TO_BE_SENT));

            var currentLocator = TrashPage.TemplateLocator + dataId + "']";
            trashPage.GetWebDriver().FindElement(By.XPath(currentLocator)).Click();
            trashPage.RemoveFromTrashMail();
            Assert.Throws<NoSuchElementException>(
                delegate
                {
                    trashPage.GetWebDriver().FindElement(By.XPath(currentLocator));
                });
        }

        // HomeTask4
        // My letter body will be placed in iframe (by default extended format)
        [Test]
        public void SentMailFromMailRuAttachFileTest()
        {
            var homePage = this.LogIn();
            var pageForWriteMail = homePage.ClickForWriteMail().TypeTo(EMAIL).TypeSubject(SUBJECT)
                .TypeBodyWithClear(TEXT_TO_BE_SENT).FileAttach(FULL_NAME_FILE);

            var homePage3 = pageForWriteMail.Send();
            var homePage4 = homePage3.ClickInbox();
            Assert.That(homePage4.GetSubject().Contains(SUBJECT));
            Assert.That(homePage4.GetMailText().Contains(TEXT_TO_BE_SENT));
        }

        private HomeMailRuPage LogIn()
        {
            this.loginPage.OpenDefaultURL(Settings.SiteUrl);
            var homePage = this.loginPage.SignIn(Settings.Login, Settings.Password);
            Assert.That(homePage.GetUserName().Contains(Settings.Login));
            return homePage;
        }
    }
}
