﻿namespace FrameWork
{
    using System.Configuration;

    public class Settings
    {
        public static string SiteUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["DefaultUrl"];
            }
        }

        public static string FileJobUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["FileJobUrl"];
            }
        }

        public static string Login
        {
            get
            {
                return ConfigurationManager.AppSettings["Login"];
            }
        }

        public static string LoginYandex
        {
            get
            {
                return ConfigurationManager.AppSettings["LoginYandex"];
            }
        }

        public static string PasswordYandex
        {
            get
            {
                return ConfigurationManager.AppSettings["PasswordYandex"];
            }
        }

        public static string Password
        {
            get
            {
                return ConfigurationManager.AppSettings["Password"];
            }
        }

        public static string BaseBrowser
        {
            get
            {
                return ConfigurationManager.AppSettings["BaseBrowser"];
            }
        }

        public static string ReserveBrowser
        {
            get
            {
                return ConfigurationManager.AppSettings["ReserveBrowser"];
            }
        }

        public static string DownloadFolder
        {
            get
            {
                return ConfigurationManager.AppSettings["DownloadFolder"];
            }
        }

        public static string Locator
        {
            // Locator (CSS or XPath)
            get
            {
                return ConfigurationManager.AppSettings["Locator"];
            }
        }
    }
}
