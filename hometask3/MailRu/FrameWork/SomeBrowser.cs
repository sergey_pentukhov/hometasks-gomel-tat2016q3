﻿namespace FrameWork
{
    using System;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Remote;

    public sealed class SomeBrowser
    {
        private const int IMPLICITLY_WAIT_TIMEOUT = 1;
        private static object threadLock = new object();
        private static SomeBrowser instance;

        private SomeBrowser(string settingBrowser = null)
        {
            switch (settingBrowser)
            {
                case "Firefox":
                    this.Driver = new RemoteWebDriver(
                        new Uri("http://127.0.0.1:4444/wd/hub"),
                                DesiredCapabilities.Firefox());
                    break;
                case "Chrome":
                    this.Driver = new ChromeDriver();
                    break;
                default:
                    this.Driver = new RemoteWebDriver(
                        new Uri("http://127.0.0.1:4444/wd/hub"),
                                DesiredCapabilities.Firefox());
            break;
        }

        this.Driver.Manage()
                .Timeouts()
                .ImplicitlyWait(
                    TimeSpan.FromSeconds(IMPLICITLY_WAIT_TIMEOUT));

            this.Driver.Manage().Window.Maximize();
        }

        public IWebDriver Driver { get; private set; }

        public string PageUrl
        {
            get
            {
                return this.Driver.Url;
            }
        }

        public static SomeBrowser GetBrowser(string settingBrowser = null)
        {
            lock (threadLock)
            {
                if (instance == null)
                {
                    instance = new SomeBrowser(settingBrowser);
                }

                return instance;
            }
        }

        public void Close()
        {
            try
            {
                if (this.Driver != null)
                {
                    this.Driver.Quit();
                }
            }
            finally
            {
                instance = null;
            }
        }
    }
}
