﻿namespace FrameWork
{
    using System;
    using System.Configuration;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.Remote;

    public sealed class SomeBrowser
    {
        private const int IMPLICITLY_WAIT_TIMEOUT = 1;
        private static object threadLock = new object();
        private static SomeBrowser instance;

        private SomeBrowser(string settingBrowser = null)
        {
            switch (settingBrowser)
            {
                case "Firefox":
                    var profile = new FirefoxProfile();

                    var capabilities = DesiredCapabilities.Firefox();

                    profile.SetPreference("browser.download.folderList", 2);
                    profile.SetPreference("browser.download.dir", ConfigurationManager.AppSettings["DownloadFolder"]);
                    profile.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");

                    capabilities.SetCapability(FirefoxDriver.ProfileCapabilityName, profile.ToBase64String());

                    this.Driver = new RemoteWebDriver(
                        new Uri("http://127.0.0.1:4444/wd/hub"), capabilities);
                    break;
                case "Chrome":
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference(
                        "download.default_directory", ConfigurationManager.AppSettings["DownloadFolder"]);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");

                    this.Driver = new ChromeDriver(chromeOptions);
                    break;
                default:
                    this.Driver = new RemoteWebDriver(
                        new Uri("http://127.0.0.1:4444/wd/hub"),
                                DesiredCapabilities.Firefox());
            break;
        }

        this.Driver.Manage()
                .Timeouts()
                .ImplicitlyWait(
                    TimeSpan.FromSeconds(IMPLICITLY_WAIT_TIMEOUT));

            this.Driver.Manage().Window.Maximize();
        }

        public IWebDriver Driver { get; private set; }

        public string PageUrl
        {
            get
            {
                return this.Driver.Url;
            }
        }

        public static SomeBrowser GetBrowser(string settingBrowser = null)
        {
            lock (threadLock)
            {
                if (instance == null)
                {
                    instance = new SomeBrowser(settingBrowser);
                }

                return instance;
            }
        }

        public void Close()
        {
            try
            {
                if (this.Driver != null)
                {
                    this.Driver.Quit();
                }
            }
            finally
            {
                instance = null;
            }
        }
    }
}
