﻿namespace FrameWork
{
    using System;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

#pragma warning disable SA1649 // File name must match first type name
    public static class PageExtensions
#pragma warning restore SA1649 // File name must match first type name
    {
        /// <summary>
        /// Find an element, waiting until a timeout is reached if necessary.
        /// </summary>
        /// <param name="context">The search context.</param>
        /// <param name="by">Method to find elements.</param>
        /// <param name="timeout">How many seconds to wait.</param>
        /// <param name="displayed">Require the element to be displayed?</param>
        /// <returns>The found element.</returns>
        public static IWebElement FindElementExtention(this ISearchContext context, By by, uint timeout, bool displayed = false)
        {
            var wait = new DefaultWait<ISearchContext>(context);
            wait.Timeout = TimeSpan.FromSeconds(timeout);
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait.Until(ctx =>
            {
                var elem = ctx.FindElement(by);
                if (displayed && !elem.Displayed)
                {
                    return null;
                }

                return elem;
            });
        }
    }

    public abstract class Page
    {
        public bool FlagAlert;
        public string TextAlert;

        protected const int PAGE_LOAD_TIMEOUT_SECONDS = 60;

        private const int ELEMENT_VISIBILITY_TIMEOUT_SECONDS = 15;
        private IWebDriver driver;

        public Page(IWebDriver webDriver)
        {
            this.driver = webDriver;

            bool isAlertPresent;
            try
            {
                this.GetWebDriver().SwitchTo().Alert();
                isAlertPresent = true;
            }
            catch (Exception)
            {
                isAlertPresent = false;
            }

            if (isAlertPresent)
            {
                this.FlagAlert = true;
                this.TextAlert = this.GetWebDriver().SwitchTo().Alert().Text;
                this.GetWebDriver().SwitchTo().Alert().Dismiss();
            }

            this.driver.Manage().Timeouts().SetPageLoadTimeout(
                                TimeSpan.FromSeconds(PAGE_LOAD_TIMEOUT_SECONDS));

            PageFactory.InitElements(this.driver, this);
        }

        public IWebDriver GetWebDriver()
        {
            return this.driver;
        }

        public void OpenDefaultURL(string startResourseUrl)
        {
            bool isAlertPresent;

            this.GetWebDriver().Navigate().GoToUrl(startResourseUrl);

            try
            {
                this.GetWebDriver().SwitchTo().Alert();
                isAlertPresent = true;
            }
            catch (Exception)
            {
                isAlertPresent = false;
            }

            if (isAlertPresent)
            {
                this.GetWebDriver().SwitchTo().Alert().Dismiss();
            }
        }

        public void WaitForElementsAppear(By locator)
        {
            new WebDriverWait(this.driver, TimeSpan.FromSeconds(ELEMENT_VISIBILITY_TIMEOUT_SECONDS))
                            .Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(locator));
        }

        public void WaitForElementsDisAppear(By locator)
        {
            new WebDriverWait(this.driver, TimeSpan.FromSeconds(ELEMENT_VISIBILITY_TIMEOUT_SECONDS))
                            .Until(ExpectedConditions.InvisibilityOfElementLocated(locator));
        }

        public void WaitForElementsIsPresent(By locator)
        {
            new WebDriverWait(this.driver, TimeSpan.FromSeconds(ELEMENT_VISIBILITY_TIMEOUT_SECONDS))
                        .Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(locator));
        }

        public bool IsVisible(IWebElement element)
        {
            try
            {
                return element.Displayed;
            }
            catch (StaleElementReferenceException)
            {
                return false;
            }
        }
    }
}
