﻿namespace FrameWork
{
    using System.Threading;
    using OpenQA.Selenium;

    public class TrashPage : DraftPage
    {
        public static By ClearFolderLocator = By.XPath("//*[@data-name='clearFolder']");
        public static By AgreeClearFolderLocator = By.XPath("//*[@class='btn btn_stylish confirm-ok']");

        public static string TemplateLocator = ".//div[@class='js-checkbox b-checkbox' and @data-id='";

        public TrashPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public DraftPage RemoveFromTrashMail()
        {
            this.GetWebDriver().FindElement(ClearFolderLocator).Click();
            Thread.Sleep(1000);
            this.GetWebDriver().FindElement(AgreeClearFolderLocator).Click();
            return new DraftPage(this.GetWebDriver());
        }
    }
}