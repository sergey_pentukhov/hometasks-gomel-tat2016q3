﻿namespace FrameWork
{
    using OpenQA.Selenium;

    public class DraftPage : SendPage
    {
        public static By FirstCheckBoxLocator = By.XPath(
            @".//div[@data-cache-key='500001_undefined_false']//div[@class='b-datalist__body']
                    /div[1]/div/a/div/div[@class='js-checkbox b-checkbox']");

        public static By RemoveButtonLocator = By.XPath(
            @".//div[@data-cache-key='500001_undefined_false']//i[@class='ico ico_toolbar ico_toolbar_remove']");

        public DraftPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public DraftPage CheckBoxMail()
        {
            this.GetWebDriver().FindElement(FirstCheckBoxLocator).Click();
            return new DraftPage(this.GetWebDriver());
        }

        public DraftPage RemoveMail()
        {
            this.GetWebDriver().FindElement(RemoveButtonLocator).Click();
            return new DraftPage(this.GetWebDriver());
        }
    }
}