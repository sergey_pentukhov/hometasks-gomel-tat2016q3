﻿/// <summary>
/// Page after sign in
/// </summary>
namespace FrameWork
{
    using System.Threading;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;

    public class HomeMailRuPage : Page
    {
        public static By HomeLabelLoginInputLocator = By.XPath("//*[@id='PH_user-email']");
        public static By WriteMailButtonLocator = By.XPath("//a[@data-name='compose']");

        public static By SubjectInFirstRowLocator = By.XPath(
            ".//div[@class='b-datalist__body']/div[1]//div[@class='b-datalist__item__subj']");

        public static By TextInFirstRowLocator = By.XPath(
            ".//div[@class='b-datalist__body']/div[1]/div[1]/a//span[@class='b-datalist__item__subj__snippet']");

        public static By OutboxLocator = By.XPath(".//*[@class='ico ico_folder ico ico_folder_send']");
        public static By InboxLocator = By.XPath(".//a[@class='b-nav__link' and @href='/messages/inbox/']");
        public static By PopupLocator = By.XPath(
            "//button[@class='btn btn_stylish btn_main confirm-ok']/span[text()='Продолжить']");

        public static By DraftLocator = By.XPath(".//*[@class='ico ico_folder ico ico_folder_drafts']");
        public static By TrashLocator = By.XPath("//a[@href='/messages/trash/']");

        public HomeMailRuPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        [FindsBy(How = How.XPath, Using = "//*[@id='PH_user-email']")]
        public IWebElement LabelUserName { get; set; }

        public string GetUserName()
        {
            this.WaitForElementsAppear(HomeLabelLoginInputLocator);
            return this.GetWebDriver().FindElement(HomeLabelLoginInputLocator).Text;
        }

        public PageForWriteMail ClickForWriteMail()
        {
            this.GetWebDriver().FindElement(WriteMailButtonLocator).Click();
            return new PageForWriteMail(this.GetWebDriver());
        }

        public HomeMailRuPage ClickInbox()
        {
            Thread.Sleep(1000);
            this.GetWebDriver().FindElement(InboxLocator).Click();
            return new HomeMailRuPage(this.GetWebDriver());
        }

        public SendPage ClickOutbox()
        {
            Thread.Sleep(1000);
            this.GetWebDriver().FindElement(OutboxLocator).Click();
            return new SendPage(this.GetWebDriver());
        }

        public DraftPage ClickDraft()
        {
            Thread.Sleep(1000);
            this.GetWebDriver().FindElement(DraftLocator).Click();
            return new DraftPage(this.GetWebDriver());
        }

        public TrashPage ClickTrash()
        {
            Thread.Sleep(1000);
            this.GetWebDriver().FindElement(TrashLocator).Click();
            return new TrashPage(this.GetWebDriver());
        }

        public HomeMailRuPage ClickPopup()
        {
            this.WaitForElementsAppear(PopupLocator);
            this.GetWebDriver().FindElement(PopupLocator).Click();
            return new HomeMailRuPage(this.GetWebDriver());
        }

        public virtual string GetSubject()
        {
            this.WaitForElementsAppear(SubjectInFirstRowLocator);
            return this.GetWebDriver().FindElement(SubjectInFirstRowLocator).Text.Trim();
        }

        public virtual string GetMailText()
        {
            this.WaitForElementsAppear(TextInFirstRowLocator);
            return this.GetWebDriver().FindElement(TextInFirstRowLocator).Text.Trim();
        }
    }
}
