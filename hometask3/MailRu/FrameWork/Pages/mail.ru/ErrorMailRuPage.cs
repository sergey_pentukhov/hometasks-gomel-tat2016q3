﻿namespace FrameWork
{
    using System;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;

    public class ErrorMailRuPage : Page
    {
        [FindsBy(How = How.XPath, Using = "//div[@class='b-login__errors']")]
        public IWebElement ErrorElement;

        public ErrorMailRuPage(IWebDriver webdriver)
            : base(webdriver)
        {
            this.GetWebDriver().Manage()
                .Timeouts()
                .ImplicitlyWait(
                    TimeSpan.FromSeconds(PAGE_LOAD_TIMEOUT_SECONDS));
        }

        public string GetErrorMessage()
        {
            return this.ErrorElement.Text;
        }
    }
}