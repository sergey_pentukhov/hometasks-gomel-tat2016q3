﻿namespace FrameWork
{
    using System.Threading;
    using OpenQA.Selenium;

    public class PageForWriteMail : Page
    {
        public static By FieldToLocator = By.XPath(
            "//textarea[@data-original-name='To']");

        public static By FieldSubjectLocator = By.XPath("//*[@name='Subject']");
        public static By BodyLocator = By.XPath(".//*[@id='tinymce']");
        public static By SendButtonLocator = By.XPath(
            ".//*[@id='b-toolbar__right']//*[@class='b-toolbar__btn b-toolbar__btn_false js-shortcut']");

        public static By FrameTextLocator = By.XPath(".//iframe[contains(@id,'composeEditor')]");
        public static By SaveButtonLocator = By.XPath(
            "//*[@data-name='saveDraft' and contains(@title,'(Ctrl+S)')]");

        public static By AttachLocator = By.XPath("//input[@name='Filedata']");

        public PageForWriteMail(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public PageForWriteMail TypeTo(string email)
        {
            this.WaitForElementsAppear(FieldToLocator);
            this.GetWebDriver().FindElement(FieldToLocator).SendKeys(email);
            return this;
        }

        public PageForWriteMail TypeSubject(string subject)
        {
            this.WaitForElementsAppear(FieldSubjectLocator);
            this.GetWebDriver().FindElement(FieldSubjectLocator).SendKeys(subject);
            return this;
        }

        public PageForWriteMail TypeBody(string body)
        {
            this.GetWebDriver().SwitchTo()
                .Frame(this.GetWebDriver().FindElement(FrameTextLocator));
            this.WaitForElementsAppear(BodyLocator);
            this.GetWebDriver().FindElement(BodyLocator).SendKeys(body);
            this.GetWebDriver().SwitchTo().ParentFrame();
            return this;
        }

        public PageForWriteMail TypeBodyWithClear(string body)
        {
            this.GetWebDriver().SwitchTo()
                .Frame(this.GetWebDriver().FindElement(FrameTextLocator));
            this.GetWebDriver().FindElement(BodyLocator).Clear();
            this.GetWebDriver().FindElement(BodyLocator).SendKeys(body);
            this.GetWebDriver().SwitchTo().ParentFrame();
            return this;
        }

        public PageForWriteMail FileAttach(string fullPathFile)
        {
            Thread.Sleep(1000);
            this.GetWebDriver().FindElement(AttachLocator).SendKeys(fullPathFile);
            return new PageForWriteMail(this.GetWebDriver());
        }

        public HomeMailRuPage Send()
        {
            this.WaitForElementsAppear(SendButtonLocator);
            this.GetWebDriver().FindElement(SendButtonLocator).Click();
            return new HomeMailRuPage(this.GetWebDriver());
        }

        public HomeMailRuPage Save()
        {
            Thread.Sleep(1000);
            this.GetWebDriver().FindElement(SaveButtonLocator).Click();
            return new HomeMailRuPage(this.GetWebDriver());
        }
    }
}