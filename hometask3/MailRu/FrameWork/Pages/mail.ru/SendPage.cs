﻿namespace FrameWork
{
    using OpenQA.Selenium;

    public class SendPage : HomeMailRuPage
    {
        public static By SubjectInSendBoxFirstRowLocator = By.XPath(
            ".//*[@id='b-letters']/div/div[5]/div/div[2]/div[1]/div/a/div[4]/div[3]/div[1]/div");

        public static By TextInSendBoxFirstRowLocator = By.XPath(
            ".//*[@id='b-letters']/div/div[5]/div/div[2]/div[1]/div/a/div[4]/div[3]/div[1]/div/span");

        public SendPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public override string GetSubject()
        {
            return this.GetWebDriver().FindElement(SubjectInSendBoxFirstRowLocator).Text.Trim();
        }

        public override string GetMailText()
        {
            return this.GetWebDriver().FindElement(TextInSendBoxFirstRowLocator).Text.Trim();
        }
    }
}
