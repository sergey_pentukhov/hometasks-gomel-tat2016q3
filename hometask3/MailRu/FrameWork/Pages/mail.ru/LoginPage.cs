﻿namespace FrameWork
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;

    public class LoginPage : Page
    {
        public static By LoginInputLocator = By.XPath("//*[@id='mailbox__login']");
        public static By PasswordInputLocator = By.XPath("//*[@id='mailbox__password']");
        public static By SubmitInputLocator = By.XPath("//*[@id='mailbox__auth__button']");

        public LoginPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        [FindsBy(How = How.XPath, Using = "//*[@id='mailbox__login']")]
        public IWebElement UserName { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='mailbox__password']")]
        public IWebElement Password { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='mailbox__auth__button']")]
        public IWebElement SubmitButton { get; set; }

        public HomeMailRuPage SignIn(string username, string password)
        {
            this.UserName.SendKeys(username);
            this.Password.SendKeys(password);
            this.SubmitButton.Click();
            return new HomeMailRuPage(this.GetWebDriver());
        }

        public ErrorMailRuPage ErrorSignIn(string username, string password)
        {
            this.UserName.SendKeys(username);
            this.Password.SendKeys(password);
            this.SubmitButton.Click();
            return new ErrorMailRuPage(this.GetWebDriver());
        }
    }
}
