﻿namespace FrameWork.Pages
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;

    public class YandexLoginPage : Page
    {
        public static By LoginInputLocator = By.XPath(".//*[@id='nb-22']");
        public static By PasswordInputLocator = By.XPath(".//*[@id='nb-23']");
        public static By SubmitInputLocator = By.XPath(".//*[@id='nb-21']");

        public YandexLoginPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        [FindsBy(How = How.XPath, Using = ".//*[@id='nb-22']")]
        public IWebElement UserName { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='nb-23']")]
        public IWebElement Password { get; set; }

        [FindsBy(How = How.XPath, Using = ".//*[@id='nb-21']")]
        public IWebElement SubmitButton { get; set; }

        public HomeYandexPage SignIn(string username, string password)
        {
            this.UserName.SendKeys(username);
            this.Password.SendKeys(password);
            this.SubmitButton.Click();
            return new HomeYandexPage(this.GetWebDriver());
        }
    }
}
