﻿namespace FrameWork.Pages
{
    using System;
    using System.Threading;
    using FrameWork;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Interactions;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    public class HomeYandexPage : Page
    {
        public static By HomeLabelLoginInputLocator = By.XPath(".//*[@class='header__username']");
        public static By WriteMailButtonLocator = By.XPath(".//input[@class='button__attach']");
        public static By CloseLocator = By.XPath("//a[@data-click-action='dialog.close']");
        public static By FileLocator = By.XPath("//div[contains(@title, 'Errors.txt')]");
        public static By DownloadButtonLocator = By.XPath(".//*[@data-click-action='resource.download']");
        public static By CleanLocator = By.XPath(".//*[text()[contains(.,'Очистить')]]");
        public static By CleanConfirmLocator = By.XPath(".//html[@id='nb-1']//span[text()='Очистить']");
        public static By PopupReUploadLocator = By.XPath("//div[contains(text(),'Загрузка')]");
        public static By RestoreLocator = By.XPath(".//*[@data-click-action='resource.restore']");

        public HomeYandexPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        [FindsBy(How = How.XPath, Using = ".//*[@class='header__username']")]
        public IWebElement LabelUserName { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@title, 'Errors.txt')]")]
        public IWebElement MyTxtFile { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[contains(@title, 'Корзина')]")]
        public IWebElement Trash { get; set; }

        public string GetUserName()
        {
            this.WaitForElementsAppear(HomeLabelLoginInputLocator);
            return this.GetWebDriver().FindElement(HomeLabelLoginInputLocator).Text;
        }

        public HomeYandexPage UploadFile(string fullPathFile)
        {
            Thread.Sleep(1000);
            this.GetWebDriver().FindElement(WriteMailButtonLocator).SendKeys(fullPathFile);
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage ClickPopupReUpload()
        {
            this.GetWebDriver().FindElement(PopupReUploadLocator).Click();
            this.GetWebDriver().FindElement(CloseLocator).Click();
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage PushDownload()
        {
            Thread.Sleep(1000);
            this.GetWebDriver().FindElement(DownloadButtonLocator).Click();
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage MarkFile()
        {
            this.WaitForElementsAppear(FileLocator);
            this.GetWebDriver().FindElement(FileLocator).Click();
            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage MoveToTrash()
        {
            this.WaitForElementsAppear(FileLocator);
            new Actions(this.GetWebDriver())
                           .ClickAndHold(this.MyTxtFile)
                           .MoveToElement(this.Trash)
                           .Release()
                           .Build()
                           .Perform();
            return new HomeYandexPage(this.GetWebDriver());
        }

        public bool IsPopupPresent()
        {
            bool isPopupReUploadPresent;
            try
            {
                this.WaitForElementsAppear(PopupReUploadLocator);
                this.GetWebDriver().FindElement(PopupReUploadLocator);
                isPopupReUploadPresent = true;
            }
            catch (NoSuchElementException)
            {
                isPopupReUploadPresent = false;
            }

            return isPopupReUploadPresent;
        }

        public HomeYandexPage ClickClean()
        {
            try
            {
                if (this.GetWebDriver().FindElement(CleanLocator).Displayed)
                {
                    this.GetWebDriver().FindElement(CleanLocator).Click();
                    this.WaitForElementsAppear(CleanConfirmLocator);
                    IWebElement element = this.GetWebDriver().FindElement(CleanConfirmLocator);
                    Actions actions = new Actions(this.GetWebDriver());
                    actions.MoveToElement(element).Click().Perform();
                }
            }
            catch (NoSuchElementException)
            {
                return new HomeYandexPage(this.GetWebDriver());
            }

            return new HomeYandexPage(this.GetWebDriver());
        }

        public HomeYandexPage ClickRestore()
        {
            this.WaitForElementsAppear(RestoreLocator);
            IWebElement element = this.GetWebDriver().FindElement(RestoreLocator);
            Actions actions = new Actions(this.GetWebDriver());
            actions.MoveToElement(element).Click().Perform();
            return new HomeYandexPage(this.GetWebDriver());
        }
    }
}