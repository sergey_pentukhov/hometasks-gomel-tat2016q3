﻿namespace FrameWork
{
    using System;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using OpenQA.Selenium.Support.UI;

    public abstract class Page
    {
        protected const int PAGE_LOAD_TIMEOUT_SECONDS = 60;

        private const int ELEMENT_VISIBILITY_TIMEOUT_SECONDS = 15;
        private IWebDriver driver;

        public Page(IWebDriver webDriver)
        {
            this.driver = webDriver;
            PageFactory.InitElements(this.driver, this);
        }

        public IWebDriver GetWebDriver()
        {
            return this.driver;
        }

        public void OpenDefaultURL()
        {
            bool isAlertPresent;

            this.GetWebDriver().Navigate().GoToUrl(Settings.SiteUrl);
            this.GetWebDriver().Manage().Timeouts().SetPageLoadTimeout(
                        TimeSpan.FromSeconds(PAGE_LOAD_TIMEOUT_SECONDS));

            try
            {
                this.GetWebDriver().SwitchTo().Alert();
                isAlertPresent = true;
            }
            catch (Exception)
            {
                isAlertPresent = false;
            }

            if (isAlertPresent)
            {
                this.GetWebDriver().SwitchTo().Alert().Dismiss();
            }
        }

        public void WaitForElementsAppear(By locator)
        {
            new WebDriverWait(this.driver, TimeSpan.FromSeconds(ELEMENT_VISIBILITY_TIMEOUT_SECONDS))
                            .Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(locator));
        }

        public void WaitForElementsDisAppear(By locator)
        {
            new WebDriverWait(this.driver, TimeSpan.FromSeconds(ELEMENT_VISIBILITY_TIMEOUT_SECONDS))
                            .Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(locator));
        }

        public void WaitForElementsIsPresent(By locator)
        {
            new WebDriverWait(this.driver, TimeSpan.FromSeconds(ELEMENT_VISIBILITY_TIMEOUT_SECONDS))
                        .Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(locator));
        }

        public bool IsVisible(IWebElement element)
        {
            try
            {
                return element.Displayed;
            }
            catch (StaleElementReferenceException)
            {
                return false;
            }
        }
    }
}
