﻿namespace FrameWork
{
    public class CurrentConfig
    {
        public string CurrentBrowser { get; set; }

        public string Suite { get; set; }

        public string Host { get; set; }

        public string Port { get; set; }

        public string Multi { get; set; }
    }
}
