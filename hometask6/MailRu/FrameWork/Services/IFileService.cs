﻿namespace FrameWork.Services
{
    using FrameWork.Pages;

    public interface IFileService
    {
        byte[] ContentFile { get; set; }

        string LocalControlSumm { get;  set; }

        void GetContentFile(string filename);

        void UploadFile(Page page, string fullNameFile);

        void DownloadFile(Page page, string fullNameFile);

        string GetMD5(byte[] hash);
    }
}