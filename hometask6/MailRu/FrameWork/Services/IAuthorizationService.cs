﻿namespace FrameWork.Services
{
    using FrameWork.BO;

    public interface IAuthorizationService
    {
        void ErrorLogin(Site service);

        void SuccessLogin(Site service);
    }
}