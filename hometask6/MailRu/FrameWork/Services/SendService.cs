﻿namespace FrameWork.Services
{
    using BO;

    public class SendService : ISendService
    {
        private Letter mail;

        public SendService()
        {
            this.mail = LetterFactory.CreateMail(LetterTypes.Full);
        }

        public SendService(LetterTypes config)
        {
            this.mail = LetterFactory.CreateMail(config);
        }

        public string GetMailText()
        {
            return this.mail.TextOfMail;
        }

        public string GetMailSubject()
        {
            return this.mail.Topic;
        }

        public ISendService ClickButtonWriteMail(Page page)
        {
            page.ClickForWriteMail();
            return this;
        }

        public ISendService FillMail(PageForWriteMail pageForWriteMail)
        {
            if (this.mail.To != null)
            {
                pageForWriteMail.TypeTo(this.mail.To);
            }

            if (this.mail.Topic != null)
            {
                pageForWriteMail.TypeSubject(this.mail.Topic);
            }

            if (this.mail.TextOfMail != null)
            {
                pageForWriteMail.TypeBodyWithClear(this.mail.TextOfMail);
            }
            else
            {
                pageForWriteMail.TypeBodyWithClear(string.Empty);
            }

            return this;
        }

        public ISendService FileAttach(PageForWriteMail pageForWritePage, string fullPathFile)
        {
            pageForWritePage.FileAttach(fullPathFile);
            return this;
        }

        public ISendService SendMail(Page homePage)
        {
            homePage.Send();
            return this;
        }
    }
}
