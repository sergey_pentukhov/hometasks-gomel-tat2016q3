﻿namespace FrameWork.Services
{
    public interface ISendService
    {
        ISendService ClickButtonWriteMail(Page page);

        ISendService FileAttach(PageForWriteMail pageForWritePage, string fullPathFile);

        ISendService FillMail(PageForWriteMail pageForWriteMail);

        string GetMailSubject();

        string GetMailText();

        ISendService SendMail(Page homePage);
    }
}