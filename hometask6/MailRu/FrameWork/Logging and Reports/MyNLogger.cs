﻿namespace FrameWork
{
    using System;
    using NLog;

    public static class MyNLogger
    {
        private static Logger logger = LogManager.GetCurrentClassLogger(typeof(MyNLogger));

        public static void Trace(string message)
        {
            logger.Trace(message);
        }

        public static void Debug(string message)
        {
            logger.Debug(message);
        }

        public static void Info(string message)
        {
            logger.Info(message);
        }

        public static void Warn(string message)
        {
            logger.Warn(message);
        }

        public static void Error(string message, Exception ex)
        {
            logger.Error(message);
        }

        public static void Fatal(string message, Exception ex)
        {
            logger.Fatal(message);
        }
    }
}
