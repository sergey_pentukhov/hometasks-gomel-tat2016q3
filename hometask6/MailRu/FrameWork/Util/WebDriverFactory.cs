﻿namespace FrameWork
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using Listeners;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.Remote;

    public enum TypeBrowser
    {
        Firefox, Chrome
    }

    public static class WebDriverFactory
    {
        private const int IMPLICITLY_WAIT_TIMEOUT = 1;
        private static string pathToSelenium = AppDomain.CurrentDomain.BaseDirectory + "../../"
                        + ConfigurationManager.AppSettings["SeleniumServerFile"];

        public static IWebDriver GetWebDriver(CurrentConfig config)
        {
            IWebDriver webDriver;
            bool remote = false;

            string hostAndPort = string.Empty;
            if (config.Host != null && config.Port != null)
            {
                hostAndPort = GetHostAndPort(config);
                remote = true;
                CreateStandAloneServer();
            }
            else
            {
                if (config.Host == null && config.Port == null)
                {
                    remote = false;
                }
                else
                {
                    MyNLogger.Debug("Not full Uri (port or url not exist)");

                    MyNLogger.Fatal(
                        "Not full Uri (port or url not exist)",
                        new Exception(
                            string.Format(
                                "http://{0}:{1}/wd/hub", config.Host ?? string.Empty, config.Port ?? string.Empty)));

                    throw new Exception(
                            string.Format(
                                "http://{0}:{1}/wd/hub", config.Host ?? string.Empty, config.Port ?? string.Empty));
                }
            }

            TypeBrowser browser = (TypeBrowser)Enum.Parse(typeof(TypeBrowser), config.CurrentBrowser);

            switch (browser)
            {
                case TypeBrowser.Firefox:
                    var profile = new FirefoxProfile();

                    var capabilities = DesiredCapabilities.Firefox();

                    profile.SetPreference("browser.download.folderList", 2);
                    profile.SetPreference("browser.download.dir", ConfigurationManager.AppSettings["DownloadFolder"]);
                    profile.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");

                    capabilities.SetCapability(FirefoxDriver.ProfileCapabilityName, profile.ToBase64String());
                    if (remote)
                    {
                        webDriver = new RemoteWebDriver(new Uri(hostAndPort), capabilities);
                    }
                    else
                    {
                        webDriver = new FirefoxDriver(profile);
                    }

                    break;

                case TypeBrowser.Chrome:
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference(
                        "download.default_directory", ConfigurationManager.AppSettings["DownloadFolder"]);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");

                    if (remote)
                    {
                        if (config.Host == null || config.Port == null)
                        {
                            MyNLogger.Fatal(
                                "Not full Uri (port or url not exist)",
                                new Exception(
                                    string.Format(
                                        "http://{0}:{1}/wd/hub", config.Host ?? string.Empty, config.Port ?? string.Empty)));
                        }

                        DesiredCapabilities capabilitiesChrome = DesiredCapabilities.Chrome();
                        webDriver = new RemoteWebDriver(new Uri(hostAndPort), capabilitiesChrome);
                    }
                    else
                    {
                        webDriver = new ChromeDriver(chromeOptions);
                    }

                    break;
                default:
                    webDriver = new RemoteWebDriver(
                        new Uri("http://127.0.0.1:4444/wd/hub"),
                                DesiredCapabilities.Firefox());
                    break;
            }

            webDriver.Manage()
                .Timeouts()
                .ImplicitlyWait(
                    TimeSpan.FromSeconds(IMPLICITLY_WAIT_TIMEOUT));

            webDriver.Manage().Window.Maximize();

            return new EventHandlerBrowserDriver(webDriver);
        }

        private static void CreateStandAloneServer()
        {
            var processInfo = new ProcessStartInfo("java.exe", "-jar " + pathToSelenium + "selenium-server-standalone-2.53.1.jar")
            {
                CreateNoWindow = true,
                UseShellExecute = false
            };
            Process proc;

            if ((proc = Process.Start(processInfo)) == null)
            {
                throw new InvalidOperationException("Selenium Standalone is not runned!");
            }
        }

        private static string GetHostAndPort(CurrentConfig config)
        {
            return string.Format("http://{0}:{1}/wd/hub", config.Host, config.Port);
        }
    }
}
