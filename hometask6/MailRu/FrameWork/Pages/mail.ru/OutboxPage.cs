﻿namespace FrameWork
{
    using OpenQA.Selenium;

    public class OutboxPage : HomeMailRuPage
    {
        public static By SubjectInOutboxFirstRowLocator = By.XPath(
            @".//div[not(contains(@style, 'display: none'))]
            /div[contains(@class,'b-datalist b-datalist_letters ')]
            /div[@class='b-datalist__body']/div[1]//div[@class='b-datalist__item__subj']");

        public static By TextInOutboxFirstRowLocator = By.XPath(
            @".//div[not(contains(@style, 'display: none'))]
            /div[contains(@class,'b-datalist b-datalist_letters ')]
            /div[@class='b-datalist__body']/div[1]//div[@class='b-datalist__item__subj']/span");

        private Element subjectInOutboxFirstRow = new Element(SubjectInOutboxFirstRowLocator);
        private Element textInOutboxFirstRow = new Element(TextInOutboxFirstRowLocator);

        public OutboxPage(IWebDriver webdriver)
            : base(webdriver)
        {
        }

        public override string GetSubject()
        {
            return this.subjectInOutboxFirstRow.GetText().Trim();
        }

        public override string GetMailText()
        {
            return this.textInOutboxFirstRow.GetText().Trim();
        }
    }
}
