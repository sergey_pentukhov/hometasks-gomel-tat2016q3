﻿namespace FrameWork
{
    using System.Configuration;
    using BO;
    using OpenQA.Selenium;
    using Pages;

    public class MailRuLoginPage : Page, ILoginPage
    {
        public static By LoginInputLocator = By.XPath("//*[@id='mailbox__login']");
        public static By PasswordInputLocator = By.XPath("//*[@id='mailbox__password']");
        public static By SubmitInputLocator = By.XPath("//*[@id='mailbox__auth__button']");

        private Element userName = new Element(LoginInputLocator);
        private Element password = new Element(PasswordInputLocator);
        private Element submitButton = new Element(SubmitInputLocator);

        public MailRuLoginPage(IWebDriver webdriver)
            : base(webdriver)
        {
            this.StartUrl = ConfigurationManager.AppSettings["DefaultUrl"];
        }

        public string StartUrl { get; set; }

        public void SignIn(Account account)
        {
            this.userName.TypeValue(account.Login);
            this.password.TypeValue(account.Password);
            this.submitButton.Click();
        }

        public void ErrorSignIn(Account account)
        {
            this.userName.TypeValue(account.Login);
            this.password.TypeValue(account.Password);
            this.submitButton.Click();
        }
    }
}
