﻿namespace FrameWork
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Interactions;
    using OpenQA.Selenium.Support.UI;

    public sealed class Browser
    {
        public const int ELEMENT_WAIT_TIMEOUT_SECONDS = 15;

        private static object threadLock = new object();

        private static ThreadLocal<Browser> instance = new ThreadLocal<Browser>();
        private IWebDriver driver;
        private CurrentConfig config;

        private Browser(CurrentConfig config = null)
        {
            this.config = config ?? this.config;
            this.Driver = WebDriverFactory.GetWebDriver(config);
            MyNLogger.Warn("Driver is - " + config.CurrentBrowser);
        }

        public IWebDriver Driver
        {
            get
            {
                MyNLogger.Trace("Driver is got");
                return this.driver;
            }

            private set
            {
                this.driver = value;
                MyNLogger.Trace("Driver is set");
            }
        }

        public string PageUrl
        {
            get
            {
                MyNLogger.Trace("Current url " + this.Driver.Url);
                return this.Driver.Url;
            }
        }

        public static Browser GetBrowser(CurrentConfig config = null/*string settingBrowser = null*/)
        {
            if (instance.Value == null)
            {
                MyNLogger.Trace("Instanse of browser is set.");
                instance.Value = new Browser(config);
            }

                return instance.Value;
        }

        public static void TakeScreenshot()
        {
            string data_time = DateTime.Now.ToString("hh_mm_ss_tt");
            string folder = DateTime.Now.ToString("yyyy-MM-dd");
            string pathToScreenShots = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../log\\", folder + "\\");
            string ext = ".png";
            try
            {
                Screenshot scr = ((ITakesScreenshot)Browser.GetBrowser(instance.Value.config).Driver).GetScreenshot();
                scr.SaveAsFile(pathToScreenShots + data_time + ext, System.Drawing.Imaging.ImageFormat.Png);
                MyNLogger.Info("Screenshot!");
            }
            catch (Exception ex)
            {
                MyNLogger.Error("Driver wasn`t got. I can`t to do screenshot..", ex);
            }
        }

        public void Close()
        {
            try
            {
                if (this.Driver != null)
                {
                    MyNLogger.Trace("Driver is closed.");
                    this.Driver.Quit();
                }
            }
            finally
            {
                MyNLogger.Warn(
                    "Driver is closed. Driver isn`t closed right. It was set up null...");
                instance.Value = null;
            }
        }

        public void Open(string url)
        {
            Uri urlFromString = null;
            try
            {
                urlFromString = new Uri(url);
            }
            catch (Exception ex)
            {
                MyNLogger.Error("Invalid format uri: " + url, ex);
            }

            this.Driver.Navigate().GoToUrl(urlFromString);

            MyNLogger.Debug(url + " is opened.");
        }

        public IWebElement FindElement(By by)
        {
            IWebElement element = null;

            try
            {
                 element = this.Driver.FindElement(by);
            }
            catch (Exception ex)
            {
                MyNLogger.Error("Element not founded: " + by, ex);
            }

            MyNLogger.Debug("Find Element: " + by);
            return element;
        }

        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            var elements = this.Driver.FindElements(by);
            if (elements.Count == 0)
            {
                MyNLogger.Warn(string.Format("Not founded elements by {0}: ", by));
            }

            MyNLogger.Debug(string.Format("Found {0} elements by {1}: ", elements.Count, by));
            return elements;
        }

        public bool IsElementPresent(By by)
        {
            return this.Driver.FindElements(by).Count() > 0;
        }

        public bool IsElementVisible(By by)
        {
            if (!this.IsElementPresent(by))
            {
                return false;
            }

            bool isVisible = this.FindElement(by).Displayed;
            string logMessage = isVisible == true ? ("Element - " + by + " - is visible")
                                            : ("Element - " + by + " - is not visible");
            MyNLogger.Debug(logMessage);

            return isVisible;
        }

        public void WaitForElementIsPresent(By by)
        {
            WebDriverWait wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(ELEMENT_WAIT_TIMEOUT_SECONDS));
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(by));
        }

        public void WaitForElementIsVisible(By by)
        {
            MyNLogger.Debug("WaitForElementIsVisible " + by);
            WebDriverWait wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(ELEMENT_WAIT_TIMEOUT_SECONDS));
            wait.Until(ExpectedConditions.ElementIsVisible(by));
        }

        public void WaitForElementIsNotVisible(By by)
        {
            MyNLogger.Debug("WaitForElementIsNotVisible " + by);
            WebDriverWait wait = new WebDriverWait(this.Driver, TimeSpan.FromSeconds(ELEMENT_WAIT_TIMEOUT_SECONDS));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(by));
        }

        public void DragAndDrop(By source, By destination)
        {
            Actions action = new Actions(this.Driver);
            action.DragAndDrop(this.FindElement(source), this.FindElement(destination));
            action.Build().Perform();
            MyNLogger.Debug("DragAndDrop " + source + "to " + destination);
        }

        public void CtrlClickElementsAndMoveTo(By destination, List<By> listUploadedFiles)
        {
            var action = new Actions(this.Driver)
                    .KeyDown(Keys.LeftControl);
            for (var i = 0; i < listUploadedFiles.Count; i++)
            {
                action.Click(this.Driver.FindElement(listUploadedFiles[i]));
            }

            action.ClickAndHold(this.Driver
                           .FindElement(listUploadedFiles[listUploadedFiles.Count - 1]))
                           .KeyUp(Keys.LeftControl)
                           .MoveToElement(this.FindElement(destination))
                           .Release()
                           .Build()
                           .Perform();
            MyNLogger.Debug("CtrlClickAllElementsAndMove to " + destination);
        }

        public bool AlertIsPresent()
        {
            bool isAlertPresent;
            try
            {
                this.Driver.SwitchTo().Alert();
                isAlertPresent = true;
                MyNLogger.Debug("Alert is present with text.." + this.Driver.SwitchTo().Alert().Text);
            }
            catch (Exception)
            {
                isAlertPresent = false;
            }

            return isAlertPresent;
        }

        public string GetAlertText()
        {
            if (this.AlertIsPresent())
            {
                MyNLogger.Debug("GetAlertText: <" + this.Driver.SwitchTo().Alert().Text + ">");
                return this.Driver.SwitchTo().Alert().Text;
            }

            return null;
        }
    }
}
