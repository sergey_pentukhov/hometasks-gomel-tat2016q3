﻿namespace MyTests
{
    using FrameWork;
    using FrameWork.BO;
    using FrameWork.Services;
    using NUnit.Framework;
    using OpenQA.Selenium;

    [Parallelizable(ParallelScope.Fixtures)]
    [TestFixture]
    public class MailRuSentMailTests : BaseTest
    {
        public const string INCORRECT_PASSWORD = "dfdfhdgjh";
        public const string INVALID_CREDENTIALS_MESSAGE_PAGE = "Неверное имя пользователя или пароль";
        public const string TEXT_TO_BE_SENT = "Hello";
        public const string SUBJECT = "Hello";
        public const string EMAIL = "1309burbolka@mail.ru";
        public const string TEXT_ALERT = "Не указан адрес";
        public const string TEXT_NO_SUBJECT = "<Без темы>";
        public const string FULL_NAME_FILE = @"d:\TAT2016\hometask3\MailRu\Errors.txt";
        public const string NAME_FILE = "Errors.txt";

        public static string TemplateFindByIdXPath = ".//div[@class='js-checkbox b-checkbox' and @data-id='";

        private IAuthorizationService authService;
        private ISendService sendService;

        public MailRuSentMailTests()
            : base()
        {
            this.authService = new AuthorizationService(Site.MailRu);
            this.sendService = new SendService();
        }

        [Test]
        public void SuccessLogInTest()
        {
            var homePage = new HomeMailRuPage(this.driver);
            this.authService.SuccessLogin(Site.MailRu);
            Assert.That(homePage.LabelUserName.GetText().Contains(Settings.LoginMailRu));
            Browser.TakeScreenshot();
        }

        [Test]
        public void FailLogInTest()
        {
            var errorPage = new ErrorMailRuPage(this.driver);
            this.authService.ErrorLogin(Site.MailRu);
            Assert.That(errorPage.GetErrorMessage().Contains(INVALID_CREDENTIALS_MESSAGE_PAGE));
            Browser.TakeScreenshot();
        }

        [Test]
        public void SendMailAndCheckInboxTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            this.sendService = new SendService();
            var homePage = new HomeMailRuPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);
            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail)
                            .SendMail(pageForWriteMail);
            homePage.ClickInbox();
            Assert.That(homePage.GetSubject().Contains(this.sendService.GetMailSubject()));
            Assert.That(homePage.GetMailText().Equals(this.sendService.GetMailText()));
            Browser.TakeScreenshot();
        }

        [Test]
        public void SendMailAndCheckOutboxTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            var homePage = new HomeMailRuPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);
            var outboxPage = new OutboxPage(this.driver);

            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail)
                                .SendMail(pageForWriteMail);

            homePage.ClickOutbox();
            Assert.That(outboxPage.GetSubject().Contains(this.sendService.GetMailSubject()));
            Assert.That(outboxPage.GetMailText().Equals(this.sendService.GetMailText()));
            Browser.TakeScreenshot();
        }

        [Test]
        public void SendMailEmptyAdressTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            this.sendService = new SendService(LetterTypes.WithoutAdress);
            var homePage = new HomeMailRuPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);

            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail)
                                .SendMail(pageForWriteMail);
            var pageForWriteMail2 = new PageForWriteMail(this.driver);

            if (pageForWriteMail2.FlagAlert)
            {
                Assert.That(pageForWriteMail2.TextAlert.Contains(TEXT_ALERT));
            }
            else
            {
                Assert.Fail("Alert isn`t presented");
            }

            Browser.TakeScreenshot();
        }

        [Test]
        public void SendMailEmptySubjectAndBodyCheckInboxTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            this.sendService = new SendService(LetterTypes.WithoutSubjectAndBody);
            var homePage = new HomeMailRuPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);
            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail)
                                   .SendMail(pageForWriteMail);

            var homePage3 = homePage.ClickPopup();
            var homePage4 = homePage3.ClickInbox();

            Assert.That(homePage4.GetSubject().Contains(TEXT_NO_SUBJECT));
            Assert.IsEmpty(homePage4.GetMailText());
            Browser.TakeScreenshot();
        }

        [Test]
        public void SendMailEmptySubjectAndBodyCheckOutboxTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            this.sendService = new SendService(LetterTypes.WithoutSubjectAndBody);
            var homePage = new HomeMailRuPage(this.driver);
            var outboxPage = new OutboxPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);

            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail)
                               .SendMail(pageForWriteMail);

            homePage.ClickPopup().ClickOutbox();
            Assert.That(outboxPage.GetSubject().Equals(TEXT_NO_SUBJECT));
            Assert.IsEmpty(outboxPage.GetMailText());
            Browser.TakeScreenshot();
        }

        // Search mail by ID
        [Test]
        public void SendMailAndSaveDraftAndRemoveTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            var homePage = new HomeMailRuPage(this.driver);
            var draftPage = new DraftPage(this.driver);
            var trashPage = new TrashPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);

            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail);
            pageForWriteMail.Save();

            homePage.ClickDraft();

            Assert.That(draftPage.GetSubject().Contains(this.sendService.GetMailSubject()));
            Browser.TakeScreenshot();

            string dataIdOfLetter = draftPage.GetWebDriver().FindElement(DraftPage.FirstCheckBoxLocator)
                    .GetAttribute("data-id");
            draftPage.CheckBoxMail();
            draftPage.RemoveMail();
            draftPage.ClickTrash();

            Assert.That(trashPage.GetSubject().Contains(this.sendService.GetMailSubject()));
            Browser.TakeScreenshot();

            string currentXPathOfLetter = TemplateFindByIdXPath + dataIdOfLetter + "']";
            trashPage.GetWebDriver().FindElement(By.XPath(currentXPathOfLetter)).Click();
            trashPage.RemoveFromTrashMail();

            Assert.Throws<NoSuchElementException>(
                delegate
                {
                    trashPage.GetWebDriver().FindElement(By.XPath(currentXPathOfLetter));
                });
            Browser.TakeScreenshot();
        }

        //// HomeTask4
        //// My letter body will be placed in iframe (by default extended format was early too)
        [Test]
        public void SendMailAttachFileTest()
        {
            this.authService.SuccessLogin(Site.MailRu);
            var homePage = new HomeMailRuPage(this.driver);
            var pageForWriteMail = new PageForWriteMail(this.driver);

            this.sendService.ClickButtonWriteMail(homePage).FillMail(pageForWriteMail)
                            .FileAttach(pageForWriteMail, FULL_NAME_FILE).SendMail(pageForWriteMail);

            homePage.ClickInbox();
            Assert.That(homePage.GetSubject().Contains(this.sendService.GetMailSubject()));
            Assert.That(homePage.GetMailText().Equals(this.sendService.GetMailText()));
            Browser.TakeScreenshot();
        }
    }
}
