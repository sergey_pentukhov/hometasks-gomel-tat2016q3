﻿namespace MyTests.Disk.Yandex.Ru
{
    using System.Collections.Generic;
    using FrameWork;
    using FrameWork.BO;
    using FrameWork.Pages;
    using FrameWork.Services;
    using NUnit.Framework;
    using OpenQA.Selenium;

    [Parallelizable(ParallelScope.Fixtures)]
    public class YandexDiskTests : BaseTest
    {
        public const string SECOND_NAME_FILE = "LICENSE.txt";
        public const string FULL_NAME_SECOND_FILE = @"d:\TAT2016\hometask3\MailRu\LICENSE.txt";

        public static By FileLocator = By.XPath("//div[contains(@title, 'Errors.txt')]");
        public static By SecondFileLocator = By.XPath("//div[contains(@title, 'LICENSE.txt')]");
        public static By TrashLocator = By.XPath("//div[contains(@title, 'Корзина')]");
        public static By LabelUserNameLocator = By.XPath("//*[@class='header__username']");

        private IFileService fileService;
        private IAuthorizationService authService;
        private Element labelUserName = new Element(LabelUserNameLocator);

        public YandexDiskTests()
            : base()
        {
            this.fileService = new FileService();
            this.authService = new AuthorizationService(Site.Yandex);
        }

        [Test]
        public void SuccessLogInYandexTest()
        {
            this.authService.SuccessLogin(Site.Yandex);
            Assert.That(this.labelUserName.GetText().Equals(Settings.LoginYandex));
            Browser.TakeScreenshot();
        }

        [Test]
        public void UploadFileAndDownloadFileTest()
        {
            this.authService.SuccessLogin(Site.Yandex);
            var homePage = new HomeYandexPage(this.driver);

            this.fileService.UploadFile(homePage, MailRuSentMailTests.FULL_NAME_FILE);

            this.fileService.GetContentFile(MailRuSentMailTests.FULL_NAME_FILE);
            string localControlSumm = this.fileService.GetMD5(this.fileService.ContentFile);

            homePage.MarkFile(FileLocator);
            this.fileService.DownloadFile(homePage, MailRuSentMailTests.FULL_NAME_FILE);

            this.fileService.GetContentFile(Settings.DownloadFolder + "\\" + MailRuSentMailTests.NAME_FILE);
            string downloadFileControlSumm = this.fileService.GetMD5(this.fileService.ContentFile);

            Assert.That(localControlSumm.Equals(downloadFileControlSumm));
            Browser.TakeScreenshot();
        }

        [Order(1)]
        [Test]
        public void UploadFileAndRemoveFileTest()
        {
            this.authService.SuccessLogin(Site.Yandex);
            var homePage = new HomeYandexPage(this.driver);

            this.fileService.UploadFile(homePage, MailRuSentMailTests.FULL_NAME_FILE);

            Assert.That(homePage.GetWebDriver().FindElement(FileLocator).GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));
            Browser.TakeScreenshot();

            homePage.GoToTrash();

            homePage.ClickClean();

            homePage.GetWebDriver().Navigate().Back();
            homePage.MoveToTrash(FileLocator);

            homePage.GoToTrash();

            homePage.GetWebDriver().Navigate().GoToUrl(homePage.GetWebDriver().Url);
            Assert.That(homePage.GetWebDriver().FindElement(FileLocator).GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));
            Browser.TakeScreenshot();
        }

        [Order(2)]
        [Test]
        public void RemoveFilePermanentlyTest()
        {
            this.authService.SuccessLogin(Site.Yandex);
            var homePage = new HomeYandexPage(this.driver);

            homePage.GoToTrash();

            Browser.GetBrowser().WaitForElementIsVisible(FileLocator);
            Assert.That(homePage.GetWebDriver().FindElement(FileLocator).GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));
            Browser.TakeScreenshot();

            homePage.ClickClean();
            homePage.GetWebDriver().Navigate().GoToUrl(homePage.GetWebDriver().Url);

                Assert.Throws<NoSuchElementException>(
                delegate
                {
                    homePage.GetWebDriver().FindElement(FileLocator);
                });

            Browser.TakeScreenshot();
        }

        [Test]
        public void UploadFileAndRemoveFileAndRestoreFileTest()
        {
            this.authService.SuccessLogin(Site.Yandex);
            var homePage = new HomeYandexPage(this.driver);

            this.fileService.UploadFile(homePage, MailRuSentMailTests.FULL_NAME_FILE);

            Assert.That(homePage.GetWebDriver().FindElement(FileLocator).GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));
            Browser.TakeScreenshot();

            homePage.MoveToTrash(FileLocator);

            homePage.GoToTrash();

            Browser.GetBrowser().WaitForElementIsVisible(FileLocator);
            homePage.MarkFile(FileLocator);
            homePage.ClickRestore();
            homePage.GetWebDriver().Navigate().Back();
            Browser.GetBrowser().WaitForElementIsVisible(FileLocator);

            Assert.That(homePage.GetWebDriver().FindElement(FileLocator).GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));
            Browser.TakeScreenshot();
        }

        [Test]
        public void RemoveSeveralFilesTest()
        {
            this.authService.SuccessLogin(Site.Yandex);
            var homePage = new HomeYandexPage(this.driver);

            this.fileService.UploadFile(homePage, MailRuSentMailTests.FULL_NAME_FILE);

            Assert.That(homePage.GetWebDriver().FindElement(FileLocator).GetAttribute("title").Equals(MailRuSentMailTests.NAME_FILE));
            Browser.TakeScreenshot();

            this.fileService.UploadFile(homePage, FULL_NAME_SECOND_FILE);

            Assert.That(homePage.GetWebDriver().FindElement(SecondFileLocator).GetAttribute("title").Equals(SECOND_NAME_FILE));
            Browser.TakeScreenshot();

            var listUploadedFiles = new List<By>();
            listUploadedFiles.Add(FileLocator);
            listUploadedFiles.Add(SecondFileLocator);

            homePage.SeveralFilesMoveToTrash(listUploadedFiles);

            Browser.GetBrowser().WaitForElementIsNotVisible(listUploadedFiles[0]);

            homePage.GoToTrash();

            foreach (var l in listUploadedFiles)
            {
                Browser.GetBrowser().WaitForElementIsVisible(l);
            }

            Assert.Pass();
            Browser.TakeScreenshot();
        }
    }
}
