// ***********************************************************************
// Copyright (c) 2015 Charlie Poole
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************

namespace NUnitLite.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using FrameWork;
    using NUnit.Common;
    using NUnitLite;
    using NUnit.Framework;

    public class Program
    {
        /// <summary>
        /// The main program executes the tests. Output may be routed to
        /// various locations, depending on the arguments passed.
        /// </summary>
        /// <remarks>Run with --help for a full list of arguments supported</remarks>
        /// <param name="args"></param>
        private const string KEY_PARALLEL = "--workers";
        private const string VALUE_PARALLEL = "=1";

        public static int Main(string[] args)
        {
            var options = new Options();

            try
            {
                if (CommandLine.Parser.Default.ParseArguments(args, options))
                {
                    if (options.Verbose)
                    {
                        if (options.Suite != null)
                        {
                            Console.WriteLine("Suite: {0}", options.Suite);
                        }

                        if (options.Host != null)
                        {
                            Console.WriteLine("Host: {0}", options.Host);
                        }

                        if (options.Port != null)
                        {
                            Console.WriteLine("Port: {0}", options.Port);
                        }

                        if (options.Multi != null)
                        {
                            Console.WriteLine("Multi: {0}", options.Multi);
                        }

                        Console.WriteLine("Browser: {0}", options.Browser);
                    }

                    var currentConfig = new CurrentConfig();
                    currentConfig.CurrentBrowser = options.Browser;
                    currentConfig.Host = options.Host;
                    currentConfig.Port = options.Port;
                    currentConfig.Suite = options.Suite;
                    currentConfig.Multi = options.Multi;

                    ConfigStart.GetCurrentConfig(currentConfig);
                }
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                return -1;
            }

            string suiteString = null;
            string[] mas = { };

            if (options.Suite != null)
            {
                string suiteFromConfig = ConfigurationManager.AppSettings[options.Suite];
                if (suiteFromConfig != null)
                {
                    suiteString = "class=" + suiteFromConfig;
                    mas = new string[] { "--where", suiteString };
                }
                else
                {
                    MyNLogger.Warn("This suite not exist!");
                    MyNLogger.Info("All tests in assembly will be done!");
                }
            }

            if (options.Multi != null)
            {
                var list = new List<string>(mas);
                list.Add(KEY_PARALLEL);
                list.Add(options.Multi);
                mas = list.ToArray();
            }

            //TestParameters cliParameters = TestContext.Parameters;
            //var t = cliParameters["b"];
            var writer = new ExtendedTextWrapper(Console.Out);
            return new AutoRun().Execute(mas, writer, Console.In);
        }
    }
}