﻿namespace NUnitLite.Tests
{
    using FrameWork;

    public static class ConfigStart
    {
        public static CurrentConfig Config { get; set; }

        public static void GetCurrentConfig(CurrentConfig conf)
        {
            Config = conf;
        }
    }
}
