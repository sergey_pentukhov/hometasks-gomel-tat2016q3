﻿using System;
using System.Diagnostics;
using System.Configuration;
using NUnitLite;

namespace ConsoleRunner
{
    public class Program
    {
        private static string pathToSelenium = AppDomain.CurrentDomain.BaseDirectory 
                        + ConfigurationManager.AppSettings["SeleniumServerFile"];

        public static int Main(string[] args)
        {
            Process.Start(pathToSelenium);
            return 0;
        }
    }
}
